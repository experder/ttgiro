// noinspection JSUnresolvedFunction

const ttgiro = {}
ttgiro.selectBuchung = function (buchung_id, sel_elem) {
    const buchungskonto = sel_elem.value;
    const div_buchungskonto = sel_elem.parentElement/*form*/.parentElement;

    div_buchungskonto.innerHTML = "...";

    tt.ajaxJson(tt_run + "/giro/api",
        "cmd=" + encodeURIComponent('selectBuchung')
        + "&buchung_id=" + encodeURIComponent(buchung_id)
        + "&buchungskonto=" + encodeURIComponent(buchungskonto)
        , function (data) {

            if (data.buchungskontoString) {
                div_buchungskonto.innerHTML = data.buchungskontoString;
            } else {
                console.log("Endpoint returned:");
                console.log(data);
                alert('Error. See console for details.');
            }
        });
}

ttgiro.setImportForm = function (selection) {
    //TODO:Spinner

    tt.ajaxJson(tt_run + "/giro/api",
        "cmd=setImportForm"
        + "&selection=" + encodeURIComponent(selection)
        , function (data) {
            if(data.file!==null){
                document.getElementById('id_file').value=data.file;
            }
            if(data.format!==null){
               document.getElementById('id_format').value=data.format;
            }
            if(data.delete!==null){
                document.getElementById('id_delete').checked=data.delete==='1';
            }
            $('select.chosen').chosen("destroy").chosen();
        });
}

ttgiro.confirmBalance = function (konto_id, month, saldo, elem) {
    tt.ajaxJson(tt_run + "/giro/api",
        "cmd=confirmBalance"
        + "&konto_id=" + encodeURIComponent(konto_id)
        + "&month=" + encodeURIComponent(month)
        + "&saldo=" + encodeURIComponent(saldo)
        , function () {
            elem.remove();
        });
}

ttgiro.confirmBalanceBilanzraum = function (bilanzraum_id, month, saldo, elem) {
    tt.ajaxJson(tt_run + "/giro/api",
        "cmd=confirmBalanceBilanzraum"
        + "&bilanzraum_id=" + encodeURIComponent(bilanzraum_id)
        + "&month=" + encodeURIComponent(month)
        + "&saldo=" + encodeURIComponent(saldo)
        , function () {
            elem.remove();
        });
}

ttgiro.afteredit = function () {
    // noinspection JSCheckFunctionSignatures - Firefox understands this 'hard reload'.
    //location.reload(true);
    location.reload();
}

ttgiro.splitUpdate = function (betrag, ausgangsbetrag) {
    betrag = tt_financial.euroToCents(betrag);
    if (betrag === false) betrag = 0;
    const betrag_split = ausgangsbetrag - betrag;
    document.getElementById('tt_split_betrag_id').value = tt_financial.centsToEuro(betrag_split);
    const elem_split = document.getElementById("tt_splitsub_id");
    const elem_countertrans = document.getElementsByClassName("ttid_giro_gegenbuchung_id")[0];
    if (betrag_split === 0) {
        elem_split.classList.add("tt_hide");
        elem_countertrans.classList.remove("tt_hide");
    } else {
        elem_split.classList.remove("tt_hide");
        elem_countertrans.classList.add("tt_hide");
    }
    $('select.chosen').chosen("destroy").chosen();
}

ttgiro.newTransaction = function (referenceNode, index) {
    const fieldset = document.createElement("fieldset");
    fieldset.innerHTML="...";
    //InsertAfter:
    referenceNode.parentNode.insertBefore(fieldset, referenceNode.nextSibling);

    tt.ajaxJson(tt_run + "/giro/api",
        "cmd=newTransactionsFormFieldset"
        + "&id=" + encodeURIComponent(index)
        , function (data) {
            fieldset.outerHTML=data.html;
            $('select.chosen').chosen("destroy").chosen();
        });
}

ttgiro.newTransactionCalc = function (textarea) {
    if(!textarea.value)return;
    const fieldset=textarea.parentElement/*div.formInput*/.parentNode/*div.tt_formfield*/.parentNode;
    const id = fieldset.getAttribute('dataid');

    //Once the new fieldset is (will be) created, remove that trigger:
    textarea.removeAttribute('onkeyup');

    ttgiro.newTransaction(fieldset, id*1+1);
}
