<?php /** @noinspection PhpUnused See RegistrationHandler::getI18wordlist() */

namespace ttgiro\v2\i18n;

use tt\features\i18n\Language;

/**
 * @see RegistrationHandler::getI18wordlist()
 */
class Lang_german extends Language
{

	/**
	 * @return string[]
	 */
	function getWordList()
	{
		return array(
			"Transaction (booking)"=>"Transaktion (Buchung)",
			"Transaction (bank)"=>"Transaktion (Bank)",
			"Bank account"=>"Bankkonto",
			"Booking account"=>"Buchungskonto",
			"Existing transactions"=>"Vorhandene Transaktionen",
			"Amount"=>"Betrag",
			"Purpose of use"=>"Verwendungszweck",
			"Offsetting entry"=>"Gegenbuchung",
			"OFFSETTING ENTRY"=>"GEGENBUCHUNG",
			"EXPECTED/PENDING/AWAITING"=>"ERWARTET",
			"Balance"=>"Saldo",
			"Inconsistencies"=>"Inkonsistenzen",
			"all"=>"alle",
			"All Transactions"=>"Alle Buchungen",
			"Pending offsetting entry"=>"Ausstehende Gegenbuchung",
			"pending offsetting entry"=>"ausstehende Gegenbuchung",
			"No booking account"=>"Kein Buchungskonto",
			"no booking account"=>"kein Buchungskonto",
			"Save settings"=>"Einstellungen speichern",
			"Settings saved"=>"Einstellungen gespeichert",
			"Manual"=>"Manuell",
			"Household budget"=>"Haushaltskasse",
			"Overview"=>"Übersicht",
			"Account"=>"Konto",
			"Bank balance"=>"Saldo (Bankkonto)",
			"Balance area"=>"Bilanzraum",
			"Missing transfer"=>"Fehlender Übertrag",
			"Sum"=>"Summe",
			"Total"=>"Gesamt",
			"No unresolved bookings"=>"Keine aufzulösenden Buchungen",
		);
	}

	/**
	 * @return string[][]
	 */
	function getWordListWithKontext()
	{
		return array(
//			Balance::LANGUAGE_CONTEXT=>array(
//				"Balance"=>"Kontostand",
//			),
		);
	}

}