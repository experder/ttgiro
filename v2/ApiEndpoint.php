<?php

namespace ttgiro\v2;

use tt\features\debug\errorhandler\v1\Error;
use tt\features\htmlpage\components\Form;
use tt\features\i18n\Trans;
use tt\features\javascripts\AjaxEndpoint;
use ttgiro\v2\features\import\Import;
use ttgiro\v2\model\AbschluesseBank;
use ttgiro\v2\model\AbschluesseBuchung;
use ttgiro\v2\model\KontoBank;
use ttgiro\v2\model\TransaktionBuchung;

class ApiEndpoint extends AjaxEndpoint
{

	public static function getClass()
	{
		/** @noinspection PhpFullyQualifiedNameUsageInspection */
		return \tt\services\polyfill\Php5::get_class();
	}

	/**
	 * @param array $input Key-value array
	 * @return array Key-value array
	 */
	public function process(array $input)
	{
		$cmd = $input['cmd'];
		if ($cmd === 'selectBuchung') {
			return array("buchungskontoString" => $this->selectBuchung($input['buchung_id'], $input['buchungskonto']));
		}
		if ($cmd === 'setImportForm') {
			return $this->setImportForm($input['selection']);
		}
		if ($cmd === 'confirmBalance') {
			return $this->confirmBalance($input['konto_id'],$input['month'],$input['saldo']);
		}
		if ($cmd === 'confirmBalanceBilanzraum') {
			return $this->confirmBalanceBilanzraum($input['bilanzraum_id'],$input['month'],$input['saldo']);
		}
		if ($cmd === 'newTransactionsFormFieldset') {
			return array("html"=>Import::newTransactionsFormFieldset(
				$input['id'],false,null,
				"ttgiro.newTransactionCalc(this);"
			)->toHtml());
		}
		new Error("Unknown command '$cmd'!");
		return array();
	}

	private function confirmBalance($konto_id, $month, $saldo){
		$abschluss = new AbschluesseBank();
		$abschluss->setDataArray(array(
			AbschluesseBank::FIELD_monat=>$month,
			AbschluesseBank::FIELD_saldo=>$saldo,
			AbschluesseBank::FIELD_bankkonto=>$konto_id,
		));
		$abschluss->persistStrict();
		return array();
	}

	private function confirmBalanceBilanzraum($bilanzraum_id, $month, $saldo){
		$abschluss = new AbschluesseBuchung();
		$abschluss->setDataArray(array(
			AbschluesseBuchung::FIELD_monat=>$month,
			AbschluesseBuchung::FIELD_saldo=>$saldo,
			AbschluesseBuchung::FIELD_bilanzraum=>$bilanzraum_id,
		));
		$abschluss->persistStrict();
		return array();
	}

	private function setImportForm($selection)
	{
		($konto=new KontoBank())->fromDbWhereEqualsId($selection);
		if(!$konto->getId())new Error("ID $selection not found!");
		return array(
			"format"=>$konto->getImportFormat(),
			"file"=>$konto->getImportFile(),
			"delete"=>$konto->getImportDelete(),
		);
	}

	private function selectBuchung($buchung_id, $buchungskonto)
	{
		$buchung = new TransaktionBuchung();
		$buchung->fromDbWhereEqualsId($buchung_id);
		if ($buchungskonto === TransaktionBuchung::SPECIAL_GEGENBUCHUNG) {
			$buchung->setAwait();
			$buchung->persistStrict();
			return "<span class='inconsistency'>".Trans::late("Pending offsetting entry")."!</span>";
		}
		if ($buchungskonto === Form::TT_NULL_VALUE) $buchungskonto = null;
		$buchung->setData(TransaktionBuchung::FIELD_buchungskonto, $buchungskonto/*?:null*/);
		$buchung->persistStrict();
		return TransaktionBuchung::getBuchungskontoName($buchungskonto, "(no booking account)");
	}

}