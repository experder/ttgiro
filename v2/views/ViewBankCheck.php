<?php

namespace ttgiro\v2\views;

use tt\features\config\v1\ConfigProject;
use tt\features\config\v1\ConfigServer;
use tt\features\css\Stylesheet;
use tt\features\database\v2\Database;
use tt\features\database\v2\querybuilder\FromTable;
use tt\features\database\v2\querybuilder\GroupbyColumn;
use tt\features\database\v2\querybuilder\JoinLeft;
use tt\features\database\v2\querybuilder\OrderbyColumn;
use tt\features\database\v2\querybuilder\WhereAND;
use tt\features\database\v2\querybuilder\WhereColumn;
use tt\features\database\v2\querybuilder\WhereOR;
use tt\features\database\v2\Schema;
use tt\features\htmlpage\view\v1\ViewHtmlNew;
use tt\features\i18n\Trans;
use tt\features\messages\v2\MsgConfirm;
use tt\services\polyfill\Php5;
use ttgiro\v2\components\TransactionBank;
use ttgiro\v2\model\schema\KontoBank2;
use ttgiro\v2\model\schema\TransaktionBank2;
use ttgiro\v2\model\schema\TransaktionBuchung2;
use ttgiro\v2\ModuleGiro;

class ViewBankCheck extends ViewHtmlNew
{

	public static function getClass() {
		return Php5::get_class();
	}

	public static function title() {
		return "🏦 Check";
	}

	/**
	 * @return string
	 */
	function getHtml() {
		$schemaTransaktionBuchung = Schema::getSingleton(TransaktionBuchung2::getClass());
		$schemaTransaktionBank = Schema::getSingleton(TransaktionBank2::getClass());
		$schemaKontoBank = Schema::getSingleton(KontoBank2::getClass());
		$colTransactionBank = $schemaTransaktionBuchung->getTableColByName(TransaktionBuchung2::COL_transaktion_bank);
		$result = Database::getGlobalDatabase()->selectGeneral1(
			array(
				$colTransactionBank,
				$schemaKontoBank->getTableColByName(KontoBank2::COL_name),
			),
			new FromTable($schemaTransaktionBuchung),
			array(
				new JoinLeft($schemaTransaktionBank, $colTransactionBank),
				new JoinLeft($schemaKontoBank, $schemaTransaktionBank->getTableColByName(TransaktionBank2::COL_konto)),
			),
			array(
				new WhereOR(
					new WhereColumn(
						WhereColumn::TYPE_EQUALS,
						$schemaTransaktionBuchung->getTableColByName(TransaktionBuchung2::COL_await),
						true
					),
					new WhereAND(
						new WhereColumn(
							WhereColumn::TYPE_EQUALS,
							$schemaTransaktionBuchung->getTableColByName(TransaktionBuchung2::COL_buchungskonto),
							null
						),
						new WhereColumn(
							WhereColumn::TYPE_EQUALS,
							$schemaTransaktionBuchung->getTableColByName(TransaktionBuchung2::COL_gegenbuchung),
							null
						)
					)
				),
			),
			array(
				new GroupbyColumn($colTransactionBank),
			),
			array(
				new OrderbyColumn($schemaTransaktionBank->getTableColByName(TransaktionBank2::COL_konto)),
			)
		);
		if(!$result){
			new MsgConfirm(Trans::late("No unresolved bookings").".");
			return "";
		}
		$html = array();
		$header = "";
		foreach ($result as $row){
			$id = $row[TransaktionBuchung2::COL_transaktion_bank];
			$transaction = TransactionBank::fromId($id);
			if($header!==$row[KontoBank2::COL_name]){
				$header=$row[KontoBank2::COL_name];
				$html[] = "<h2>$header</h2>";
			}
			$html[] = $transaction->transactionHtml();
		}
		return implode("\n",$html);
	}

	/**
	 * @return string
	 */
	function getTitle() {
		return self::title();
	}

	public function getCss()
	{
		return array(ModuleGiro::getCss(),
			new Stylesheet(
				ConfigServer::$HTTP_MODULE_ROOT[ModuleGiro::$module_id] . '/v2/css/'.ConfigProject::$SKIN_ID
				.'/overview.css'
				,ConfigServer::$HTTP_MODULE_ROOT[ModuleGiro::$module_id] . '/v2/css/'.ConfigProject::$SKIN_ID
				.'/nightmode_overview.css'
				,ConfigServer::$HTTP_MODULE_ROOT[ModuleGiro::$module_id] . '/v2/css/'.ConfigProject::$SKIN_ID
				.'/mobile_overview.css'
			),
		);
	}

	public function getJsUrls()
	{
		return array(
			ConfigServer::$HTTP_MODULE_ROOT[ModuleGiro::$module_id] . "/v2/giro.js",
		);
	}


}