<?php

namespace ttgiro\v2\views;

use DateInterval;
use DateTime;
use Exception;
use tt\features\debug\errorhandler\v1\Error;
use ttgiro\v2\components\MonthSelector;
use ttgiro\v2\model\AbschluesseBank;

class OverviewMonthSelector extends MonthSelector
{

	private $konto_id;

	private $saldo_next;
	private $uebertrag;

	public function __construct($konto_id)
	{
		parent::__construct();
		$this->konto_id=$konto_id;
		$this->saldo_next = AbschluesseBank::fromDbByMonat($this->month, $this->konto_id);
		$this->uebertrag = AbschluesseBank::fromDbByMonat($this->vormonat, $this->konto_id);
	}

	protected function checkVormonat(){
		try {
			$vorvormonat = date("Y-m", date_sub(new DateTime($this->vormonat . '-01'),
				new DateInterval("P1M"))->getTimestamp());
			if(!AbschluesseBank::fromDbByMonat($vorvormonat, $this->konto_id))return false;
		} catch (Exception $e) {
			Error::fromException($e);
		}
		return true;
	}
	public function checkFolgemonat(){
		if(!$this->saldo_next)return false;
		return true;
	}

	public function checkNextSaldo($kontostand)
	{
		if(!$this->saldo_next)return true;
		return $this->saldo_next->getSaldo()==$kontostand;
	}

	/**
	 * @return false|AbschluesseBank
	 */
	public function getUebertrag()
	{
		return $this->uebertrag;
	}

	/**
	 * @return int|mixed
	 */
	public function getUebertragSaldo($else = 0)
	{
		if(!$this->uebertrag)return $else;
		return $this->uebertrag->getSaldo();
	}

}