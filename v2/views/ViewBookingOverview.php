<?php

namespace ttgiro\v2\views;

use tt\features\config\v1\CFG_S;
use tt\features\config\v1\ConfigServer;
use tt\features\database\v1\DatabaseHandler;
use tt\features\database\v1\db_mysql\DatabaseMySql;
use tt\features\database\v1\OrderClause;
use tt\features\database\v1\WhereEquals;
use tt\features\debug\errorhandler\v2\Warning;
use tt\features\htmlpage\components\table\SimpleTable;
use tt\features\htmlpage\components\table\SimpleTableRow;
use tt\features\htmlpage\view\v1\ViewHtmlNew;
use tt\features\i18n\Trans;
use tt\services\polyfill\Php5;
use tt\services\polyfill\Php7;
use tt\services\ServiceDateTime;
use tt\services\ServiceFinancial;
use tt\services\UnicodeIcons;
use ttgiro\v2\components\MonthSelector;
use ttgiro\v2\model\AbschluesseBuchung;
use ttgiro\v2\model\Bilanzraum;
use ttgiro\v2\model\KontoBuchung;
use ttgiro\v2\ModuleGiro;
use ttgiro\v2\ServiceGiro;
use ttgiro\v2\UnicodeIcons as GiroUnicode;

class ViewBookingOverview extends ViewHtmlNew
{

	public static function getClass() {
		return Php5::get_class();
	}

	/**
	 * @return string
	 */
	function getHtml()
	{
		if(!isset($_GET[MonthSelector::GETVAL_month]))$_GET[MonthSelector::GETVAL_month]=date("Y-m");
		$db = DatabaseHandler::getDefaultDb();

		$monthSelector = new BalanceMonthSelector();
		$month = $monthSelector->getMonth();
		$monthYMD = $monthSelector->getMonthYMD();
		$month_name = ServiceDateTime::monthAusgeschrieben($month);
		$vormonat_name = ServiceDateTime::monthAusgeschrieben($monthSelector->getVormonat());
		$folgemonat = $monthSelector->getFolgemonatYMD();

		$alltheTables = array();
		$sum_all_bilanzraeume_lastmonth = 0;
		$sum_diff_thismonth = 0;

		$tableTotal = new SimpleTable(array(), array("",$vormonat_name,$month_name,""));

		foreach (Bilanzraum::getFromDb() as $bilanzraum){

			$bilanzraum_id=$bilanzraum->getId();
			$bilanzraum_title = $bilanzraum->getName()
				.(CFG_S::$DEVMODE?" (#$bilanzraum_id)":"")
			;

			$table = array();
			$sum_vormonat = 0;
			$diff_current = 0;

			$abschluss = AbschluesseBuchung::fromDbByMonat($monthSelector->getVormonat(), $bilanzraum_id);
			if($abschluss===false){
					new Warning(Trans::late("Missing transfer").": ".$bilanzraum_title);
			}else{
				$sum_vormonat=$abschluss->getSaldo();
			}
			$abschluss_next = AbschluesseBuchung::fromDbByMonat($month, $bilanzraum_id);

			foreach (KontoBuchung::getFromDb(array(
				new WhereEquals(KontoBuchung::FIELD_bilanzraum, $bilanzraum_id),
				new OrderClause(KontoBuchung::FIELD_orderby, false),
				new OrderClause(KontoBuchung::FIELD_name),
			)) as $kontoBuchung){
				if(!($kontoBuchung instanceof KontoBuchung))continue;
				$konto_id = $kontoBuchung->getId();

				$link = $kontoBuchung->getName()
					.(CFG_S::$DEVMODE?" (#".$kontoBuchung->getId().")":"")
				;

				if($db instanceof DatabaseMySql)
				$result = $db->select("SELECT sum(betrag) s
	FROM giro_transaktion_buchung
	where datum>=:monat and datum<:folgemonat
	and buchungskonto=:konto",array(
		":monat"=>$monthYMD,
		":folgemonat"=>$folgemonat,
		":konto"=>$konto_id,
				));
				$sum_month = $result?$result[0]['s']:0;

				$diff_current+=$sum_month;

				$row = array(
					$link,
					"",//ServiceFinancial::centsToEuro($uebertrag),
					$sum_month?ServiceGiro::centsToEuroHtml($sum_month):"",
					"",//$betrag,
				);
				if($sum_month)
				$table[] = $row;
			}

			$sum_diff_thismonth += $diff_current;
			$sum_all_bilanzraeume_lastmonth += $sum_vormonat;
			$sum_month = $diff_current+$sum_vormonat;

			if(count($table)<2)$table=array();
			$tableComponent = new SimpleTable($table, false);//, array("",$vormonat_name,$month_name,""));
			$tableComponent->addClass('balance');

			$check_betrag=$abschluss_next===false&&$month<date("Y-m")?"<span class='rightOverflowVisible'>&nbsp;"
				."<a class='button' onclick=\""
				."ttgiro.confirmBalanceBilanzraum('$bilanzraum_id', '$month', '$sum_month', this);"
				."\">".Php7::mb_chr(GiroUnicode::seal)."</a>"
				."</span>":"";

			$sum_row = new SimpleTableRow(array(
				"<span class='balance_sum'>".Trans::late("Sum")." $bilanzraum_title</span>",
				ServiceFinancial::centsToEuro($sum_vormonat),
				$diff_current?ServiceGiro::centsToEuroHtml($diff_current):"",
				ServiceFinancial::centsToEuro($sum_month).$check_betrag,
			));
			$sum_row->addClass('balance_sum');
			$tableComponent->add($sum_row);
			if($bilanzraum_id==1){
				$sum_row = new SimpleTableRow(array(
					"<span class='balance_sum'>".Trans::late("Sum")." $bilanzraum_title</span>",
					ServiceFinancial::centsToEuro($sum_vormonat),
					$diff_current?ServiceGiro::centsToEuroHtml($diff_current):"",
					ServiceFinancial::centsToEuro($sum_month),
				));
				$sum_row->addClass('balance_sum');
				$tableTotal->add($sum_row);
			}

			$alltheTables[] = "<h3>$bilanzraum_title</h3>";
			$alltheTables[] = $tableComponent->toHtml();

		}

		$sum = $sum_all_bilanzraeume_lastmonth+$sum_diff_thismonth;

		$sum_check = $db->select("SELECT sum(saldo) s
				FROM giro_abschluesse
				where monat=:monat",array(
			":monat"=>$month,
		));
		$check_betrag="<span class='rightOverflowVisible'>&nbsp;"
			.($sum_check[0]['s']==$sum?Php7::mb_chr(UnicodeIcons::Heavy_Check_Mark):Php7::mb_chr(UnicodeIcons::cross_mark))
			."</span>";

		$tableTotal->addClass('balance');
		$sum_row = new SimpleTableRow(array(
			"<span class='balance_sum'>".Trans::late("Total")."</span>",
			ServiceFinancial::centsToEuro($sum_all_bilanzraeume_lastmonth),
			$sum_diff_thismonth?ServiceGiro::centsToEuroHtml($sum_diff_thismonth):"",
			ServiceFinancial::centsToEuro($sum).$check_betrag,
		));
		$tableTotal->add($sum_row);

		array_unshift($alltheTables, $tableTotal->toHtml());

		return $monthSelector->viaGetval()
				.implode("\n", $alltheTables)
			;
	}

	/**
	 * @return string
	 */
	function getTitle()
	{
		return self::title();
	}

	public static function title(){
		return Php7::mb_chr(GiroUnicode::receipt).' '.Trans::late("Overview");
	}

	public function getCss()
	{
		return array(ModuleGiro::getCss());
	}

	/**
	 * @return string[]|null
	 */
	public function getJsUrls(){
		return array(
			ConfigServer::$HTTP_MODULE_ROOT[ModuleGiro::$module_id] . '/v2/giro.js',
		);
	}

	/**
	 * @return string
	 */
	public function getCssClass(){
		return "booking_overview";
	}

}