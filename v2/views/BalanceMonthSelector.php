<?php

namespace ttgiro\v2\views;

use ttgiro\v2\components\MonthSelector;

class BalanceMonthSelector extends MonthSelector
{

	protected function checkVormonat(){
		return $this->vormonat<=date("Y-m");
	}
	protected function checkFolgemonat(){
		return $this->month<date("Y-m");
	}

}