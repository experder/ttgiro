<?php

namespace ttgiro\v2\views;

use DateInterval;
use DateTime;
use Exception;
use tt\features\config\v1\ConfigProject;
use tt\features\config\v1\ConfigServer;
use tt\features\css\Stylesheet;
use tt\features\database\v1\DatabaseHandler;
use tt\features\database\v1\LeftJoinStatement;
use tt\features\database\v1\LimitClause;
use tt\features\database\v1\OrderClause;
use tt\features\database\v2\Database;
use tt\features\database\v2\querybuilder\FromTable;
use tt\features\database\v2\querybuilder\Orderby;
use tt\features\database\v2\querybuilder\OrderbyColumn;
use tt\features\database\v2\querybuilder\SelectColumn;
use tt\features\database\v2\querybuilder\WhereColumn;
use tt\features\database\v2\Schema;
use tt\features\debug\errorhandler\v1\Error;
use tt\features\htmlpage\components\FormInputDropdown;
use tt\features\htmlpage\components\HtmlComponent;
use tt\features\htmlpage\view\v1\ViewHtmlNew;
use tt\features\i18n\Trans;
use tt\services\polyfill\Php5;
use tt\services\polyfill\Php7;
use tt\services\ServiceEnv;
use tt\services\ServiceFinancial;
use tt\services\UNI;
use tt\services\UnicodeIcons;
use ttgiro\v2\components\MonthSelector;
use ttgiro\v2\components\TransactionBank;
use ttgiro\v2\model\AbschluesseBank;
use ttgiro\v2\model\KontoBank;
use ttgiro\v2\model\schema\TransaktionBank2;
use ttgiro\v2\model\TransaktionBank;
use ttgiro\v2\ModuleGiro;
use ttgiro\v2\UnicodeIcons as GiroUnicode;

class ViewBankAccount extends ViewHtmlNew
{

	public $prefixWithTitle = false;

	const GETVAL_konto = 'konto';

	/**
	 * @var KontoBank|false $konto
	 */
	private $konto = false;

	public function __construct()
	{
		if ($konto = ServiceEnv::valueFromGet(self::GETVAL_konto, false)) {
			$this->konto = new KontoBank();
			$this->konto->fromDbWhereEqualsId($konto);
		}
		if ($this->konto === false) $this->preselectKonto();
	}

	public static function getClass() {
		return Php5::get_class();
	}

	/**
	 * @return string
	 */
	public function getHtml()
	{
		return $this->htmlOverview();
	}

	/**
	 * @return FormInputDropdown
	 */
	private function kontoSelector()
	{
		$konten_query = KontoBank::getFromDb();
		$options = array();
		$selected = null;
		foreach ($konten_query as $konto) {
			if (!($konto instanceof KontoBank)) continue;
			$id = $konto->getId();
			$url = ServiceEnv::updateUrlParam(self::GETVAL_konto, $id);
			$options[$url] = $konto->getName();
			if ($id === $this->konto->getId()) $selected = $url;
		}
		$dropdown = new FormInputDropdown("x", $options, false, $selected);
		$dropdown->addKeyVal(HtmlComponent::KEY_ONCHANGE, "location.href=this.value");
		return $dropdown;
	}

	public static function queryAllTransactions($konto_id, $from, $to){
		$transaktionBankSchema = Schema::getSingleton(TransaktionBank2::getClass());
		return Database::getGlobalDatabase()->selectGeneral1(
			array(
				$transaktionBankSchema->getTableColPrimary(),
				new SelectColumn($transaktionBankSchema->getTableColByName(TransaktionBank2::COL_betrag)),
				$transaktionBankSchema->getTableColByName(TransaktionBank2::COL_datum),
				$transaktionBankSchema->getTableColByName(TransaktionBank2::COL_verwendungszweck),
			),
			new FromTable($transaktionBankSchema),
			array(),
			array(
				new WhereColumn(WhereColumn::TYPE_EQUALS,
					$transaktionBankSchema->getTableColByName(TransaktionBank2::COL_konto),
					$konto_id),
				new WhereColumn(WhereColumn::TYPE_GREATER_EQUAL,
					$transaktionBankSchema->getTableColByName(TransaktionBank2::COL_datum),
					$from),
				new WhereColumn(WhereColumn::TYPE_SMALLER,
					$transaktionBankSchema->getTableColByName(TransaktionBank2::COL_datum),
					$to),
			),
			array(),
			array(
				new OrderbyColumn($transaktionBankSchema->getTableColByName(TransaktionBank2::COL_datum), Orderby::DESC),
				new OrderbyColumn($transaktionBankSchema->getTableColPrimary(), Orderby::DESC),
			)
		);
	}

	private function htmlOverview() {
		$monthSelector = new OverviewMonthSelector($this->konto->getId());

		$uebertrag = $monthSelector->getUebertrag();
		if (!$uebertrag) new Error("No transfer (Übertrag,Abschluss) found for '".$monthSelector->getVormonat()."'!");

		$transactions_html = $this->transactionsHtml(
			$monthSelector->getMonthYMD(), $monthSelector->getFolgemonatYMD(), $sum);

		$kontostand_html = $this->kontostandHtml($sum + $uebertrag->getSaldo(), $monthSelector);

		$html = array();
		$html[] = "<h1>".Php7::mb_chr(GiroUnicode::Bank).' '.$this->kontoSelector()->toHtml()."</h1>";
		$html[] = $monthSelector->viaGetval();
		$html[] = $kontostand_html;
		$html[] = implode("\n", $transactions_html);

		return implode("\n", $html);
	}

	private function kontostandHtml($kontostand, OverviewMonthSelector $monthSelector) {
		$kontostand_check = $monthSelector->checkNextSaldo($kontostand) ? ""
			:(' '.UNI::asEmoji(UnicodeIcons::High_Voltage_Sign));
		$kontostand_check .= (!$monthSelector->checkFolgemonat() && $monthSelector->getMonth()<date("Y-m"))
			?" ".UNI::asEmoji(GiroUnicode::Bank).'?'
			:"";
		$kontostand_html = ServiceFinancial::centsToEuro($kontostand) . $kontostand_check;
		return "<h3>" .Trans::late("Balance").": " . $kontostand_html . "</h3>";
	}

	private function transactionsHtml($from, $to, &$sum = 0) {
		$transactions_html = array();
		foreach (self::queryAllTransactions($this->konto->getId(), $from, $to) as $row) {
			$transactions_html[] = $this->transactionHtml($row);
			$sum += $row[TransaktionBank::FIELD_betrag];
		}
		return $transactions_html;
	}

	private function transactionHtml($row)
	{
		$transaction = TransactionBank::fromRow($row);
		return $transaction->transactionHtml();
	}

	/**
	 * @return string
	 */
	public function getTitle()
	{
		if($this->konto){
			return Php7::mb_chr(GiroUnicode::Bank).' '.$this->konto->getName();
		}
		return self::title();
	}
	public static function title()
	{
		return Php7::mb_chr(GiroUnicode::Bank).' '.Trans::late("Account");
	}

	public function getCss()
	{
		return array(ModuleGiro::getCss(),
			new Stylesheet(
				ConfigServer::$HTTP_MODULE_ROOT[ModuleGiro::$module_id] . '/v2/css/'.ConfigProject::$SKIN_ID
				.'/overview.css'
				,ConfigServer::$HTTP_MODULE_ROOT[ModuleGiro::$module_id] . '/v2/css/'.ConfigProject::$SKIN_ID
				.'/nightmode_overview.css'
				,ConfigServer::$HTTP_MODULE_ROOT[ModuleGiro::$module_id] . '/v2/css/'.ConfigProject::$SKIN_ID
				.'/mobile_overview.css'
			),
		);
	}

	public function getJsUrls()
	{
		return array(
			ConfigServer::$HTTP_MODULE_ROOT[ModuleGiro::$module_id] . "/v2/giro.js",
		);
	}

	private function preselectKonto()
	{
		$db = DatabaseHandler::getDefaultDb();
		$last_saldo = $db->generalQuery(array(AbschluesseBank::FIELD_bankkonto,AbschluesseBank::FIELD_monat),
		AbschluesseBank::table_name,array(
				new LeftJoinStatement(KontoBank::$table_name, AbschluesseBank::FIELD_bankkonto),
				//Last month, first account:
				new OrderClause(AbschluesseBank::FIELD_monat, false),
				new OrderClause(KontoBank::FIELD_orderby, false),
				new OrderClause(KontoBank::FIELD_name),
				new LimitClause(1),
			),"359");

		if(!$last_saldo)new Error("Please add first saldo!");
		$konto = $last_saldo[0][AbschluesseBank::FIELD_bankkonto];
		$vormonat = $last_saldo[0][AbschluesseBank::FIELD_monat];
		$monat=null;
		try {
			$monat = date("Y-m", date_add(new DateTime($vormonat), new DateInterval("P1M"))->getTimestamp());
		} catch (Exception $e) {
			Error::fromException($e);
		}

		($this->konto = new KontoBank())->fromDbWhereEqualsId($_GET[self::GETVAL_konto] = $konto);
		$_GET[MonthSelector::GETVAL_month] = $monat;
	}

	public function getCssClass()
	{
		return "giro_overview";
	}

}