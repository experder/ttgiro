<?php

namespace ttgiro\v2\views;

use tt\features\config\v1\ConfigServer;
use tt\features\database\v1\DatabaseHandler;
use tt\features\database\v1\db_mysql\DatabaseMySql;
use tt\features\debug\errorhandler\v2\Warning;
use tt\features\frontcontroller\bycode\ServiceRoutes;
use tt\features\htmlpage\components\Html_A;
use tt\features\htmlpage\components\table\SimpleTable;
use tt\features\htmlpage\components\table\SimpleTableRow;
use tt\features\htmlpage\view\v1\ViewHtmlNew;
use tt\features\i18n\Trans;
use tt\services\polyfill\Php7;
use tt\services\ServiceDateTime;
use tt\services\ServiceFinancial;
use tt\services\ServiceHtml;
use tt\services\UNI;
use tt\services\UnicodeIcons;
use ttgiro\v2\components\MonthSelector;
use ttgiro\v2\model\KontoBank;
use ttgiro\v2\ModuleGiro;
use ttgiro\v2\ServiceGiro;
use ttgiro\v2\UnicodeIcons as GiroUnicode;

class ViewBankOverview extends ViewHtmlNew
{

	public static function getClass()
	{
		/** @noinspection PhpFullyQualifiedNameUsageInspection */
		return \tt\services\polyfill\Php5::get_class();
	}

	/**
	 * @return string
	 */
	function getHtml()
	{
		if(!isset($_GET[MonthSelector::GETVAL_month]))$_GET[MonthSelector::GETVAL_month]=date("Y-m");
		$db = DatabaseHandler::getDefaultDb();

		$monthSelector = new BalanceMonthSelector();
		$month = $monthSelector->getMonth();
		$month_name = ServiceDateTime::monthAusgeschrieben($month);
		$vormonat_name = ServiceDateTime::monthAusgeschrieben($monthSelector->getVormonat());

		$sum_overall = 0;
		$sum_vormonat = 0;
		$table = array();
		$ok=true;
		$ok_betrag=true;
		$ok_bank=true;
		foreach (KontoBank::getFromDb() as $konto){
			if(!($konto instanceof KontoBank))continue;
			$konto_id = $konto->getId();

			$monthSelector_inner = new OverviewMonthSelector($konto_id);

			$link = new Html_A(ServiceRoutes::getLinkHref(ViewBankAccount::getClass())
				."?".ViewBankAccount::GETVAL_konto.'='.$konto_id
				."&".MonthSelector::GETVAL_month.'='.$month
				, $konto->getName());

			if($db instanceof DatabaseMySql)
			$result = $db->select("SELECT sum(betrag) s
FROM giro_transaktion_konto
where datum>=:monat and datum<:folgemonat
and konto=:konto",array(
	":monat"=>$monthSelector_inner->getMonthYMD(),
	":folgemonat"=>$monthSelector_inner->getFolgemonatYMD(),
	":konto"=>$konto_id,
			));
			$sum_month = $result?($result[0]['s']?:0):0;

			$uebertrag = $monthSelector_inner->getUebertragSaldo(false);
			if($uebertrag===false){
				new Warning(Trans::late("Missing transfer").": ".$konto->getName());
				$ok=false;
				continue;
			}
			$saldo_month=$uebertrag+$sum_month;
			$sum_overall+=$saldo_month;
			$sum_vormonat+=$uebertrag;
			$betrag = ServiceFinancial::centsToEuro($saldo_month);

			$sum_warnings = "";
			if(!$monthSelector_inner->checkNextSaldo($saldo_month)){
				$ok_betrag=false;
				$sum_warnings.=UNI::asEmoji(UnicodeIcons::High_Voltage_Sign);
			}
			if(!$monthSelector_inner->checkFolgemonat() && $monthSelector->getMonth()<date("Y-m")){
				$ok_bank=false;
				$sum_warnings.=Php7::mb_chr(GiroUnicode::Bank).'?';
				$sum_warnings.=$this->confirmBalance($konto_id, $month, $saldo_month);
			}
			if($saldo_month<0){
				$sum_warnings.=UNI::asEmoji(UnicodeIcons::Warning_Sign);
			}
			$betrag .= $sum_warnings?"<span class='rightOverflowVisible'>&nbsp;".$sum_warnings."</span>":"";

			$row = array(
				$link->toHtml(),
				ServiceFinancial::centsToEuro($uebertrag),
				$sum_month?ServiceGiro::centsToEuroHtml($sum_month):"",
				$betrag,
			);
			$table[] = $row;
		}

		$diff = $sum_overall-$sum_vormonat;

		$header=array("",$vormonat_name,$month_name,"");
		$tableComponent = new SimpleTable($table, $header);
		$tableComponent->addClass('balance');

		if($ok){
			$check_betrag="";
			if(!$ok_bank)$check_betrag="<span class='rightOverflowVisible'>&nbsp;"
				.Php7::mb_chr(GiroUnicode::Bank)."?</span>";
			if(!$ok_betrag)$check_betrag="<span class='rightOverflowVisible'>&nbsp;"
				.Php7::mb_chr(UnicodeIcons::High_Voltage_Sign)."</span>";
			$sum_row = new SimpleTableRow(array(
				"<span class='balance_sum'>".Trans::late("Sum")."</span>",
				ServiceFinancial::centsToEuro($sum_vormonat),
				$diff?ServiceGiro::centsToEuroHtml($diff):"",
				ServiceFinancial::centsToEuro($sum_overall).$check_betrag,
			));
			$sum_row->addClass('balance_sum');
			$tableComponent->add($sum_row);
		}

		return $monthSelector->viaGetval()
				.$tableComponent->toHtml()
			;
	}

	private function confirmBalance($konto_id, $month, $saldo){
		return ServiceHtml::checkAction("ttgiro.confirmBalance('$konto_id', '$month', '$saldo', this);");
	}

	/**
	 * @return string
	 */
	function getTitle()
	{
		return self::title();
	}

	public static function title(){
		return Php7::mb_chr(GiroUnicode::Bank).' '.Trans::late("Overview");
	}

	public function getCss()
	{
		return array(ModuleGiro::getCss());
	}

	/**
	 * @return string[]|null
	 */
	public function getJsUrls(){
		return array(
			ConfigServer::$HTTP_MODULE_ROOT[ModuleGiro::$module_id] . '/v2/giro.js',
		);
	}

}