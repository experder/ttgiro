<?php

namespace ttgiro\v2\model;

use tt\features\database\schema\Table;
use tt\features\database\v1\Model;
use tt\features\database\v1\OrderClause;
use tt\features\i18n\Trans;
use tt\services\polyfill\Php5;

class Bilanzraum extends Model
{

	const table_name = "giro_bilanzraum";
	const gui_name = "Balance area";

	protected $tableName = self::table_name;

	/**
	 * @var string $name
	 */
	protected $name;
	const FIELD_name = 'name';

	/**
	 * @var int $orderby
	 */
	protected $orderby;
	const FIELD_orderby = 'orderby';

	public static function getClass()
	{
		return Php5::get_class();
	}

	function getGuiName()
	{
		return Trans::late(self::gui_name);
	}

	/**
	 * @param Table $table
	 * @return void
	 */
	function changeDefaultSchema(Table $table)
	{
		$table->getColumn(self::FIELD_orderby)
			->setDataTypeInteger()
		;
		$table->getColumn(self::FIELD_name)
			->setNotNullable()
			->addIndexUnique($table)
			;
	}

	/**
	 * @return string
	 */
	public function getName() {
		return $this->name;
	}

	/**
	 * @param array $whereJoinOrder
	 * @return Bilanzraum[]
	 * @noinspection PhpReturnDocTypeMismatchInspection
	 */
	public static function getFromDb($whereJoinOrder=true){
		if($whereJoinOrder===true){
			$whereJoinOrder=array(
				new OrderClause(Bilanzraum::FIELD_orderby, false),
				new OrderClause(Bilanzraum::FIELD_name),
			);
		}
		/** @noinspection PhpIncompatibleReturnTypeInspection */
		return Model::fromDatabase(Bilanzraum::getClass(), $whereJoinOrder);
	}

}