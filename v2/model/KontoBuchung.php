<?php /** @noinspection PhpUnused TODO */

namespace ttgiro\v2\model;

use tt\features\config\v1\CFG_S;
use tt\features\database\schema\Table;
use tt\features\database\v1\DatabaseHandler;
use tt\features\database\v1\LeftJoinStatement;
use tt\features\database\v1\Model;
use tt\features\database\v1\OrderClause;
use tt\features\database\v1\WhereEquals;
use tt\features\i18n\Trans;
use tt\services\polyfill\Php5;
use tt\services\ServiceArrays;
use ttgiro\v2\model\schema\KontoBuchung2;

class KontoBuchung extends Model
{

	public static $table_name = "giro_kategorie";
	const gui_name = "Booking account";

	/**
	 * @var string $name
	 */
	protected $name;
	const FIELD_name = 'name';

	/**
	 * @var int $orderby
	 */
	protected $orderby;
	const FIELD_orderby = 'orderby';

	/**
	 * @var int $bilanzraum
	 */
	protected $bilanzraum;
	const FIELD_bilanzraum = 'bilanzraum';

	public static function getFromDb($whereJoinOrder = true) {
		if ($whereJoinOrder === true) {
			$whereJoinOrder = array(
				new OrderClause(KontoBuchung::FIELD_orderby, false),
				new OrderClause(KontoBuchung::FIELD_name),
			);
		}
		return Model::fromDatabase(KontoBuchung::getClass(), $whereJoinOrder);
	}

	public static function getClass() {
		return Php5::get_class();
	}

	public function getTableName() {
		return self::$table_name;
	}

	/**
	 * @param Table $table
	 * @return void
	 */
	function changeDefaultSchema(Table $table) {
		$name = $table->getColumn(self::FIELD_name)
			->setNotNullable();
		$table->getColumn(self::FIELD_orderby)
			->setDataTypeInteger();
		$bilanzraum = $table->getColumn(self::FIELD_bilanzraum)
			->setDataTypeInteger()
			->setNotNullable()
			->addForeignKey($table, Bilanzraum::table_name);
		$table->addIndexUnique(array($name, $bilanzraum));
	}

	function getGuiName() {
		return Trans::late(self::gui_name);
	}

	public function getDefaultFormat() {
		return "{bname} / {" . self::FIELD_name . "}" . (CFG_S::$DEVMODE ? " (#{" . Model::FIELD_id . "})" : "");
	}

	public function getDefaultFormattedList($whereOrder = array()) {
		if (count($whereOrder) == 0) $whereOrder = array(
			new WhereEquals(KontoBuchung2::COL_show, 1),
			new OrderClause('t0.' . self::FIELD_orderby, false),
			new OrderClause('bname'),
			new OrderClause('t0.' . self::FIELD_name),//TODO:!!!!!!
		);

		$whereOrder = array_merge($whereOrder, array(
			new LeftJoinStatement(Bilanzraum::table_name, self::FIELD_bilanzraum),
		));

		$db = DatabaseHandler::getDefaultDb();
		$keys = array(
			Model::FIELD_id,
			self::FIELD_name,
			Bilanzraum::table_name . '.' . Bilanzraum::FIELD_name . ' as bname',
		);
		$options_result = $db->generalQuery(
			$keys,
			$this->getTableName(),
			$whereOrder,
			107
		);

		return ServiceArrays::keyValueArray($options_result, $this->getDefaultFormat());
	}

	/**
	 * @return string
	 */
	public function getName() {
		return $this->name;
	}

}