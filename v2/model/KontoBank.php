<?php /** @noinspection PhpUnused TODO */

namespace ttgiro\v2\model;

use tt\features\database\schema\Column;
use tt\features\database\schema\Table;
use tt\features\database\v1\Model;
use tt\features\database\v1\OrderClause;
use tt\features\i18n\Trans;

class KontoBank extends Model
{

	public static $table_name = "giro_konto";
	const gui_name = "Bank account";

	protected $name;
	const FIELD_name = 'name';
	protected $desc;
	const FIELD_desc = 'desc';
	protected $iban;
	const FIELD_iban = 'iban';
	protected $import_format;
	const FIELD_import_format = 'import_format';
	protected $import_file;
	const FIELD_import_file = 'import_file';
	protected $import_delete;
	const FIELD_import_delete = 'import_delete';
	protected $orderby;
	const FIELD_orderby = 'orderby';

	public static function getClass()
	{
		/** @noinspection PhpFullyQualifiedNameUsageInspection */
		return \tt\services\polyfill\Php5::get_class();
	}

	public function getTableName()
	{
		return self::$table_name;
	}

	function changeDefaultSchema(Table $table)
	{
		$table->getColumn(self::FIELD_name)
			->setNotNullable()
			->addIndexUnique($table);
		$table->getColumn(self::FIELD_desc)->setDataType(Column::DATATYPE_TEXT);
		$table->getColumn(self::FIELD_iban)->setStringMaxLength(40);
		$table->getColumn(self::FIELD_import_format);
		$table->getColumn(self::FIELD_import_file);
		$table->getColumn(self::FIELD_import_delete)->setDataType(Column::DATATYPE_BOOLEAN);
		$table->getColumn(self::FIELD_orderby)->setDataTypeInteger();
	}

	/**
	 * @return string
	 */
	public function getImportFormat()
	{
		return $this->import_format;
	}

	/**
	 * @return string
	 */
	public function getImportFile()
	{
		return $this->import_file;
	}

	/**
	 * @return bool
	 */
	public function getImportDelete()
	{
		return $this->import_delete;
	}

	/**
	 * @return string
	 */
	public function getName()
	{
		return $this->name;
	}

	function getGuiName()
	{
		return Trans::late(self::gui_name);
	}

	public static function getFromDb($whereJoinOrder=true){
		if($whereJoinOrder===true){
			$whereJoinOrder=array(
				new OrderClause(KontoBank::FIELD_orderby, false),
				new OrderClause(KontoBank::FIELD_name),
			);
		}
		return Model::fromDatabase(KontoBank::getClass(), $whereJoinOrder);
	}

}