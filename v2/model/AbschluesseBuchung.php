<?php /** @noinspection PhpUnused TODO */

namespace ttgiro\v2\model;

use tt\features\database\schema\Table;
use tt\features\database\v1\Model;

class AbschluesseBuchung extends Model
{

	const table_name = "giro_bilanzraum_abschluesse";
	protected $tableName = self::table_name;

	protected $bilanzraum;
	const FIELD_bilanzraum = 'bilanzraum';
	protected $monat;
	const FIELD_monat = 'monat';
	protected $saldo;
	const FIELD_saldo = 'saldo';

	public static function fromDbByMonat($month, $bilanzraum_id)
	{
		$abschluss = new self();
		$ok = $abschluss->fromDbWhereEquals(array(
			self::FIELD_monat => $month,
			self::FIELD_bilanzraum => $bilanzraum_id,
		));
		if (!$ok) return false;
		return $abschluss;
	}

	function getGuiName()
	{
		return "????36";
	}

	/**
	 * @param Table $table
	 * @return void
	 */
	function changeDefaultSchema(Table $table)
	{
		$table->getColumn(self::FIELD_bilanzraum)
			->setNotNullable()
			->setDataTypeInteger()
			->addForeignKey($table, Bilanzraum::table_name);
		$table->getColumn(self::FIELD_monat)
			->setNotNullable();
		$table->getColumn(self::FIELD_saldo)
			->setNotNullable()
			->setDataTypeInteger();
		$table->addIndexUnique(array(
			$table->getColumn(self::FIELD_bilanzraum),
			$table->getColumn(self::FIELD_monat),
		));
	}

	/**
	 * @return int
	 */
	public function getSaldo()
	{
		return $this->saldo;
	}

}