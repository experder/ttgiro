<?php /** @noinspection PhpUnused TODO */

namespace ttgiro\v2\model;

use tt\features\database\schema\Column;
use tt\features\database\schema\Table;
use tt\features\database\v1\Model;

class DkbHashLog extends Model
{

	public static $table_name = "giro_dkb_log";
	const gui_name = "dkb_log";

	/**
	 * @var TransaktionBank $transaktion
	 */
	protected $transaktion;
	const VAL_transaktion = 'transaktion';
	protected $import_date;
	const VAL_import_date = 'import_date';
	protected $verwendungszweck;
	const VAL_verwendungszweck = 'verwendungszweck';
	protected $hash;
	const VAL_hash = 'hash';

	/**
	 * @param TransaktionBank $transaktion
	 * @return DkbHashLog
	 */
	public static function fromTransaction(TransaktionBank $transaktion)
	{
		$log = new DkbHashLog();
		$log->setDataArray(array(
			self::VAL_transaktion => $transaktion,
			self::VAL_import_date => date("Y-m-d H:i:s"),
			self::VAL_verwendungszweck => $transaktion->getVerwendungszweck(),
			self::VAL_hash => $transaktion->getHash(),
		));
		return $log;
	}

	public function getTableName()
	{
		return self::$table_name;
	}

	function changeDefaultSchema(Table $table)
	{
		$table->getColumn(self::VAL_transaktion)
			->setDataTypeInteger()
			->addForeignKey($table, TransaktionBank::table_name)
			->setNotNullable();
		$table->getColumn(self::VAL_import_date)
			->setDataType(Column::DATATYPE_DATETIME)
			->setNotNullable();
		$table->getColumn(self::VAL_verwendungszweck)
			->setDataType(Column::DATATYPE_TEXT);
	}

	function getGuiName()
	{
		return self::gui_name;
	}

}