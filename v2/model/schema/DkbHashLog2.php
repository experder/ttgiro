<?php

namespace ttgiro\v2\model\schema;

use tt\features\database\v2\Schema;
use tt\features\database\v2\SchemaColumn;
use tt\services\polyfill\Php5;

class DkbHashLog2 extends Schema
{

	const COL_transaktion = 'transaktion';
	const COL_import_date = 'import_date';
	const COL_verwendungszweck = 'verwendungszweck';
	const COL_hash = 'hash';

	public static function getClass() {
		return Php5::get_class();
	}

	/**
	 * @return string
	 */
	function getTableName()
	{
		return "giro_dkb_log";
	}

	/**
	 * @return SchemaColumn[]
	 */
	function createTableCols()
	{
		return array(
			($x=new SchemaColumn($this, self::COL_transaktion, SchemaColumn::DATATYPE_INTEGER))
				->setNotNullable()
				->addForeignKey(TransaktionBank2::getClass())
			,
			($x=new SchemaColumn($this, self::COL_import_date, SchemaColumn::DATATYPE_DATETIME))
				->setNotNullable()
			,
			($x=new SchemaColumn($this, self::COL_verwendungszweck, SchemaColumn::DATATYPE_TEXT))
			,
			($x=new SchemaColumn($this, self::COL_hash, SchemaColumn::DATATYPE_STRING))
			,
		);
	}

}