<?php

namespace ttgiro\v2\model\schema;

use tt\features\database\v2\Database;
use tt\features\database\v2\querybuilder\FromTable;
use tt\features\database\v2\querybuilder\Limit;
use tt\features\database\v2\querybuilder\Orderby;
use tt\features\database\v2\querybuilder\OrderbyColumn;
use tt\features\database\v2\querybuilder\WhereColumn;
use tt\features\database\v2\Schema;
use tt\features\database\v2\SchemaColumn;
use tt\features\database\v2\SchemaConstraintUniquekey;
use tt\services\polyfill\Php5;

class AbschluesseBank2 extends Schema
{

	const COL_bankkonto = 'bankkonto';
	const COL_monat = 'monat';
	const COL_saldo = 'saldo';

	public static function getClass() {
		return Php5::get_class();
	}

	/**
	 * @return string
	 */
	function getTableName()
	{
		return "giro_abschluesse";
	}

	/**
	 * @return SchemaColumn[]
	 */
	function createTableCols()
	{
		$cols = array(
			($bankkonto=new SchemaColumn($this, self::COL_bankkonto, SchemaColumn::DATATYPE_INTEGER))
				->setNotNullable()
				->addForeignKey(KontoBank2::getClass())
			,
			($monat=new SchemaColumn($this, self::COL_monat, SchemaColumn::DATATYPE_STRING))
				->setNotNullable()
			,
			($x=new SchemaColumn($this, self::COL_saldo, SchemaColumn::DATATYPE_INTEGER))
				->setNotNullable()
			,
		);
		$this->addConstraint(new SchemaConstraintUniquekey(array($bankkonto,$monat)));
		return $cols;
	}

	public static function latestAbschluss($konto_id){
		$schemaAbschluesse = Schema::getSingleton(AbschluesseBank2::getClass());
		$latest_abschluss = Database::getGlobalDatabase()->selectGeneral1(
			array(
				$schemaAbschluesse->getTableColByName(AbschluesseBank2::COL_saldo),
				$schemaAbschluesse->getTableColByName(AbschluesseBank2::COL_monat)
			),
			new FromTable($schemaAbschluesse),
			array(),
			array(
				new WhereColumn(WhereColumn::TYPE_EQUALS,
					$schemaAbschluesse->getTableColByName(AbschluesseBank2::COL_bankkonto), $konto_id),
			),
			array(),
			array(
				new OrderbyColumn($schemaAbschluesse->getTableColByName(AbschluesseBank2::COL_monat), Orderby::DESC),
			),
			new Limit(1)
		);
		if (!$latest_abschluss) return false;
		return $latest_abschluss[0];
	}

}