<?php

namespace ttgiro\v2\model\schema;

use tt\features\database\v2\Schema;
use tt\features\database\v2\SchemaColumn;
use tt\features\database\v2\SchemaConstraintUniquekey;
use tt\services\polyfill\Php5;

class KontoBuchung2 extends Schema
{

	const COL_name = 'name';
	const COL_orderby = 'orderby';
	const COL_bilanzraum = 'bilanzraum';
	const COL_show = 'show';

	public static function getClass() {
		return Php5::get_class();
	}

	/**
	 * @return string
	 */
	function getTableName() {
		return "giro_kategorie";
	}

	/**
	 * @return SchemaColumn[]
	 */
	function createTableCols() {
		$cols = array(
			($name = new SchemaColumn($this, self::COL_name, SchemaColumn::DATATYPE_STRING))
				->setNotNullable()
		,
			(new SchemaColumn($this, self::COL_orderby, SchemaColumn::DATATYPE_INTEGER))
		,
			($bilanzraum = new SchemaColumn($this, self::COL_bilanzraum, SchemaColumn::DATATYPE_INTEGER))
				->setNotNullable()
				->addForeignKey(Bilanzraum2::getClass())
		,
			(new SchemaColumn($this, self::COL_show, SchemaColumn::DATATYPE_BOOLEAN))
		,
		);
		$this->addConstraint(new SchemaConstraintUniquekey(array($name, $bilanzraum)));
		return $cols;
	}

}