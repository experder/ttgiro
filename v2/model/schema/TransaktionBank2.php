<?php

namespace ttgiro\v2\model\schema;

use tt\features\database\v2\Schema;
use tt\features\database\v2\SchemaColumn;
use tt\features\database\v2\SchemaConstraintIndex;
use tt\services\polyfill\Php5;

class TransaktionBank2 extends Schema
{

	const COL_datum = 'datum';
	const COL_konto = 'konto';
	const COL_betrag = 'betrag';
	const COL_verwendungszweck = 'verwendungszweck';
	const COL_hash = 'hash';

	public static function getClass() {
		return Php5::get_class();
	}

	/**
	 * @return string
	 */
	function getTableName()
	{
		return "giro_transaktion_konto";
	}

	/**
	 * @return SchemaColumn[]
	 */
	function createTableCols()
	{
		$cols = array(
			($datum=new SchemaColumn($this, self::COL_datum, SchemaColumn::DATATYPE_DATE))
			->setNotNullable()
			,
			($konto=new SchemaColumn($this, self::COL_konto, SchemaColumn::DATATYPE_INTEGER))
			->setNotNullable()
			->addForeignKey(KontoBank2::getClass())
			,
			($x=new SchemaColumn($this, self::COL_betrag, SchemaColumn::DATATYPE_INTEGER))
			->setNotNullable()
			,
			($x=new SchemaColumn($this, self::COL_verwendungszweck, SchemaColumn::DATATYPE_TEXT))
			->setNotNullable()
			,
			($x=new SchemaColumn($this, self::COL_hash, SchemaColumn::DATATYPE_STRING))
			->setNotNullable()
			,
		);
		$this->addConstraint(new SchemaConstraintIndex(array($datum)));
		return $cols;
	}

}