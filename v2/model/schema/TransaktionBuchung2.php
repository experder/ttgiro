<?php
/**  @noinspection PhpUnusedLocalVariableInspection TODO */
/**  @noinspection PhpUnusedPrivateMethodInspection TODO */
/**  @noinspection PhpUnused TODO */
/**  @noinspection PhpPropertyOnlyWrittenInspection TODO */

namespace ttgiro\v2\model\schema;

use tt\features\database\v2\Schema;
use tt\features\database\v2\SchemaColumn;
use tt\services\polyfill\Php5;

class TransaktionBuchung2 extends Schema
{

	const COL_datum = "datum";
	const COL_transaktion_bank = 'transaktion_bank';
	const COL_betrag = 'betrag';
	const COL_verwendungszweck = 'verwendungszweck';
	const COL_buchungskonto = 'buchungskonto';
	const COL_gegenbuchung = 'gegenbuchung';
	const COL_await = 'await';

	public static function getClass() {
		return Php5::get_class();
	}

	/**
	 * @return SchemaColumn[]
	 */
	function createTableCols()
	{
		return array(
			($x = new SchemaColumn($this, self::COL_datum, SchemaColumn::DATATYPE_DATE))
				->setNotNullable()
		,
			($x = new SchemaColumn($this, self::COL_transaktion_bank, SchemaColumn::DATATYPE_INTEGER))
				->setNotNullable()
				->addForeignKey(TransaktionBank2::getClass())
		,
			($x = new SchemaColumn($this, self::COL_betrag, SchemaColumn::DATATYPE_INTEGER))
				->setNotNullable()
		,
			($x = new SchemaColumn($this, self::COL_verwendungszweck, SchemaColumn::DATATYPE_TEXT))
		,
			($x = new SchemaColumn($this, self::COL_buchungskonto, SchemaColumn::DATATYPE_INTEGER))
				->addForeignKey(KontoBuchung2::getClass())
		,
			($x = new SchemaColumn($this, self::COL_gegenbuchung, SchemaColumn::DATATYPE_INTEGER))
				->addForeignKey(TransaktionBuchung2::getClass())
		,
			($x = new SchemaColumn($this, self::COL_await, SchemaColumn::DATATYPE_BOOLEAN))
				->setNotNullable()
				->setDefault(false)
		,
		);
	}

	/**
	 * @return string
	 */
	function getTableName()
	{
		return "giro_transaktion_buchung";
	}

}