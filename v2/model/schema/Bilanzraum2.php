<?php

namespace ttgiro\v2\model\schema;

use tt\features\database\v2\Schema;
use tt\features\database\v2\SchemaColumn;
use tt\services\polyfill\Php5;

class Bilanzraum2 extends Schema
{

	const COL_name = 'name';
	const COL_orderby = 'orderby';

	public static function getClass() {
		return Php5::get_class();
	}

	/**
	 * @return string
	 */
	function getTableName()
	{
		return "giro_bilanzraum";
	}

	/**
	 * @return SchemaColumn[]
	 */
	function createTableCols()
	{
		return array(
			($x=new SchemaColumn($this, self::COL_name, SchemaColumn::DATATYPE_STRING))
				->setNotNullable()
				->addIndexUnique()
			,
			($x=new SchemaColumn($this, self::COL_orderby, SchemaColumn::DATATYPE_INTEGER))
			,
		);
	}

}