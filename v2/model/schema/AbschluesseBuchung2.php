<?php

namespace ttgiro\v2\model\schema;

use tt\features\database\v2\Schema;
use tt\features\database\v2\SchemaColumn;
use tt\features\database\v2\SchemaConstraintUniquekey;
use tt\services\polyfill\Php5;

class AbschluesseBuchung2 extends Schema
{

	const COL_bilanzraum = 'bilanzraum';
	const COL_monat = 'monat';
	const COL_saldo = 'saldo';

	public static function getClass() {
		return Php5::get_class();
	}

	/**
	 * @return string
	 */
	function getTableName()
	{
		return "giro_bilanzraum_abschluesse";
	}

	/**
	 * @return SchemaColumn[]
	 */
	function createTableCols()
	{
		$cols = array(
			($bilanzraum=new SchemaColumn($this, self::COL_bilanzraum, SchemaColumn::DATATYPE_INTEGER))
				->setNotNullable()
				->addForeignKey(Bilanzraum2::getClass())
			,
			($monat=new SchemaColumn($this, self::COL_monat, SchemaColumn::DATATYPE_STRING))
				->setNotNullable()
			,
			($x=new SchemaColumn($this, self::COL_saldo, SchemaColumn::DATATYPE_INTEGER))
				->setNotNullable()
			,
		);
		$this->addConstraint(new SchemaConstraintUniquekey(array($bilanzraum,$monat)));
		return $cols;
	}

}