<?php /** @noinspection PhpUnusedLocalVariableInspection PHP 5.4 */

namespace ttgiro\v2\model\schema;

use tt\features\database\v2\Schema;
use tt\features\database\v2\SchemaColumn;
use tt\services\polyfill\Php5;

class KontoBank2 extends Schema
{

	const COL_name = 'name';
	const COL_desc = 'desc';
	const COL_iban = 'iban';
	const COL_import_format = 'import_format';
	const COL_import_file = 'import_file';
	const COL_import_delete = 'import_delete';
	const COL_orderby = 'orderby';
	const COL_latest_import = 'latest_import';

	public static function getClass() {
		return Php5::get_class();
	}

	/**
	 * @return string
	 */
	function getTableName()
	{
		return "giro_konto";
	}

	/**
	 * @return SchemaColumn[]
	 */
	function createTableCols()
	{
		return array(
			($x=new SchemaColumn($this, self::COL_name, SchemaColumn::DATATYPE_STRING))
				->setNotNullable()
				->addIndexUnique()
			,
			($x=new SchemaColumn($this, self::COL_desc, SchemaColumn::DATATYPE_TEXT))
			,
			($x=new SchemaColumn($this, self::COL_iban, SchemaColumn::DATATYPE_STRING))
				->setMaxLength(40)
			,
			($x=new SchemaColumn($this, self::COL_import_format, SchemaColumn::DATATYPE_STRING))
			,
			($x=new SchemaColumn($this, self::COL_import_file, SchemaColumn::DATATYPE_STRING))
			,
			($x=new SchemaColumn($this, self::COL_import_delete, SchemaColumn::DATATYPE_BOOLEAN))
			,
			($x=new SchemaColumn($this, self::COL_orderby, SchemaColumn::DATATYPE_INTEGER))
			,
			($x=new SchemaColumn($this, self::COL_latest_import, SchemaColumn::DATATYPE_DATE))
			,
		);
	}

}