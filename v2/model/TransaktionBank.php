<?php

namespace ttgiro\v2\model;

use tt\features\config\v1\CFG_S;
use tt\features\database\schema\Column;
use tt\features\database\schema\ServiceSchema;
use tt\features\database\schema\Table;
use tt\features\database\v1\Model;
use tt\features\i18n\Trans;
use tt\services\ServiceEnv;
use tt\services\ServiceFinancial;

class TransaktionBank extends Model
{

	const table_name = "giro_transaktion_konto";
	const gui_name = "Transaction (bank)";

	/**
	 * @var string $datum Y-m-d
	 */
	protected $datum;
	const FIELD_datum = 'datum';
	/**
	 * @var int $konto
	 */
	protected $konto;
	const FIELD_konto = 'konto';
	/**
	 * @var integer $betrag (signed) cents
	 */
	protected $betrag;
	const FIELD_betrag = 'betrag';
	/**
	 * @var string $verwendungszweck
	 */
	protected $verwendungszweck;
	const FIELD_verwendungszweck = 'verwendungszweck';
	/**
	 * @var string $hash
	 */
	protected $hash = "";
	const FIELD_hash = 'hash';

	function getGuiName()
	{
		return Trans::late(self::gui_name);
	}

	/**
	 * @param Table $table
	 * @return void
	 */
	function changeDefaultSchema(Table $table)
	{
		$table->getColumn(self::FIELD_datum)
			->setGuiName(Trans::late("Date"))
			->setDataType(Column::DATATYPE_DATE)
			->setNotNullable();
		$table->getColumn(self::FIELD_konto)
			->setGuiName(Trans::late("Bank account"))
			->setDataTypeInteger()
			->setNotNullable()
			->addForeignKey($table, KontoBank::$table_name);
		$table->getColumn(self::FIELD_betrag)
			->setGuiName(Trans::late("Amount"))
			->setDataTypeInteger()
			->setNotNullable();
		$table->getColumn(self::FIELD_verwendungszweck)
			->setGuiName(Trans::late("Purpose of use"))
			->setDataType(Column::DATATYPE_TEXT)
			->setNotNullable();
		$table->getColumn(self::FIELD_hash)
			->setNotNullable();

		//Optimization:
		$table->addIndex(ServiceSchema::columnsByNames(array(self::FIELD_konto)));
		$table->addIndex(ServiceSchema::columnsByNames(array(self::FIELD_datum)));
		#Consider this:
		#$table->addIndex(ServiceSchema::columnsByNames(array(self::FIELD_datum,Model::FIELD_id)));

	}

	public function getTableName()
	{
		return self::table_name;
	}

	public static function fromImport($datum, $betrag, $text, $konto = null)
	{
		$text = ServiceEnv::normalizeLinebreaks($text);

		$transaction = new TransaktionBank();

		$transaction->setDataArray(array(
			self::FIELD_datum => $datum,
			self::FIELD_betrag => $betrag,
			self::FIELD_verwendungszweck => $text,
			self::FIELD_konto => $konto,
		));

		return $transaction;
	}

	/**
	 * @return string
	 */
	public function calcHash()
	{
		return md5($this->datum . $this->betrag . "|" . $this->verwendungszweck);
	}

	/**
	 * @return TransaktionBank
	 */
	public function setHash()
	{
		$this->hash = $this->calcHash();
		return $this;
	}

	/**
	 * @return string
	 */
	public function getHash()
	{
		return $this->hash;
	}

	/**
	 * @return int
	 */
	public function getKonto()
	{
		return $this->konto;
	}

	/**
	 * @return string
	 */
	public function getDatum()
	{
		return $this->datum;
	}

	/**
	 * @return string
	 */
	public function getDatumReadable()
	{
		return $this->getDataToReadable(self::FIELD_datum);
	}

	/**
	 * @return int
	 */
	public function getBetrag()
	{
		return $this->betrag;
	}

	/**
	 * @param string $datum
	 * @return TransaktionBank
	 */
	public function setDatum($datum)
	{
		$this->datum = $datum;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getVerwendungszweck()
	{
		return $this->verwendungszweck;
	}

	public function getDefaultFormat()
	{
		return "Konto {" . self::FIELD_konto . "}, {" . self::FIELD_betrag . "} ({" . self::FIELD_datum . "})"
			. (CFG_S::$DEVMODE ? " (#{" . Model::FIELD_id . "})" : "");
	}

	public function toHtml()
	{
		$konto = new KontoBank();
		$konto->fromDbWhereEqualsId($this->getKonto());
		return date("d.m.", strtotime($this->getDatum()))
			. " " . ServiceFinancial::centsToEuro($this->getBetrag())
			. " " . $konto->getName()
			. (CFG_S::$DEVMODE ? " (#" . $this->getId() . ")" : "")
			. "<br>" . htmlentities($this->getVerwendungszweck());
	}

}