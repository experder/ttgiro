<?php /** @noinspection PhpUnused TODO */

namespace ttgiro\v2\model;

use tt\features\config\v1\CFG_S;
use tt\features\config\v1\ConfigServer;
use tt\features\database\schema\Column;
use tt\features\database\schema\ForeignKey;
use tt\features\database\schema\Table;
use tt\features\database\v1\DatabaseHandler;
use tt\features\database\v1\LeftJoinStatement;
use tt\features\database\v1\Model;
use tt\features\database\v1\views\EditModel;
use tt\features\database\v1\WhereCondition;
use tt\features\database\v1\WhereEquals;
use tt\features\debug\errorhandler\v2\Warning;
use tt\features\frontcontroller\bycode\ServiceRoutes;
use tt\features\htmlpage\components\Form;
use tt\features\htmlpage\components\FormDiv;
use tt\features\htmlpage\components\FormInputDropdown;
use tt\features\htmlpage\components\FormInputText;
use tt\features\htmlpage\components\FormInputTextarea;
use tt\features\htmlpage\components\FormSubset;
use tt\features\htmlpage\components\FunctionEmoji;
use tt\features\htmlpage\components\HtmlCompFormInput;
use tt\features\htmlpage\components\HtmlComponent;
use tt\features\i18n\Trans;
use tt\features\messages\v2\MsgConfirm;
use tt\features\messages\v2\MsgError;
use tt\services\polyfill\Php7;
use tt\services\ServiceEnv;
use tt\services\ServiceFinancial;
use tt\services\ServiceStrings;
use tt\services\UNI;
use tt\services\UnicodeIcons;
use ttgiro\v2\ModuleGiro;

class TransaktionBuchung extends Model
{

	const table_name = "giro_transaktion_buchung";
	const gui_name = "Transaction (booking)";
	protected $tableName = self::table_name;

	const SPECIAL_GEGENBUCHUNG = 'gegenbuchung';
	const GEGENBUCHUNG_AWAIT = 'await';

	const POSTVAL_SPLIT_DATUM = 'split_datum';
	const POSTVAL_SPLIT_VERWENDUNGSZWECK = 'split_text';
	const POSTVAL_SPLIT_BUCHUNGSKONTO = 'split_buchungskonto';

	/**
	 * @var string $datum Y-m-d
	 */
	protected $datum;
	const FIELD_datum = 'datum';

	/**
	 * @var TransaktionBank $konto
	 */
	protected $transaktion_bank;
	const FIELD_transaktion_bank = 'transaktion_bank';

	/**
	 * @var integer $betrag (signed) cents
	 */
	protected $betrag;
	const FIELD_betrag = 'betrag';

	/**
	 * @var string $verwendungszweck
	 */
	protected $verwendungszweck;
	const FIELD_verwendungszweck = 'verwendungszweck';

	/**
	 * @var int $buchungskonto
	 */
	protected $buchungskonto;
	const FIELD_buchungskonto = 'buchungskonto';

	/**
	 * @var int $gegenbuchung
	 */
	protected $gegenbuchung = null;
	const FIELD_gegenbuchung = 'gegenbuchung';

	/**
	 * @var boolean $await
	 */
	protected $await = false;
	const FIELD_await = 'await';

	private static $alle_buchungskonten = null;

	public static function getClass()
	{
		/** @noinspection PhpFullyQualifiedNameUsageInspection */
		return \tt\services\polyfill\Php5::get_class();
	}

	public static function getBuchungskontoName($buchungskonto_id, $else = false)
	{
		if (!$buchungskonto_id || $buchungskonto_id === Form::TT_NULL_VALUE) return $else;
		$alle_buchungskonten = self::getAlleBuchungskonten();
		return $alle_buchungskonten[$buchungskonto_id]??"---";
	}

	public static function getAlleBuchungskonten($special = false)
	{
		if (self::$alle_buchungskonten === null) {
			$kat = new KontoBuchung();
			self::$alle_buchungskonten = $kat->getDefaultFormattedList();
		}
		if ($special) {
			return array(
					Form::TT_NULL_VALUE => "(unknown)",
					self::SPECIAL_GEGENBUCHUNG => ">>> ".Trans::late("OFFSETTING ENTRY")." <<<",
				) + self::$alle_buchungskonten;
		}
		return self::$alle_buchungskonten;
	}

	public static function htmlSelector($buchung_id)
	{
		$options = TransaktionBuchung::getAlleBuchungskonten(true);
		$input = new FormInputDropdown('x', $options, false);
		$input->addKeyVal(HtmlComponent::KEY_ONCHANGE, "ttgiro.selectBuchung('$buchung_id',this);");
		return "<form class='buchung_selector'>" . ($input->toHtml()) . "</form>";
	}

	public static function htmlEditOption($buchung_id)
	{
		$a = ServiceRoutes::getLinkComponent(EditModel::getClass(), UNI::emoji(UnicodeIcons::Pencil), array(
			EditModel::PARAM_classname => self::getClass(),
			EditModel::PARAM_id => $buchung_id,
		));
		$a->addClassButton();
		$a->addKeyVal(HtmlComponent::KEY_TITLE, 'Edit transaction');
//		$href = $a->removeHref();
//		$a->addKeyVal(HtmlComponent::KEY_ONCLICK, "tt.dialog('" . ($href) . "',ttgiro.afteredit);");
		return $a->toHtml();
	}

	/**
	 * @return TransaktionBuchung
	 */
	public function setAwait()
	{
		$this->await = true;
		return $this;
	}

	/**
	 * @param Table $table
	 * @return void
	 */
	function changeDefaultSchema(Table $table)
	{
		$table->getColumn(self::FIELD_datum)
			->setGuiName(Trans::late("Date"))
			->setNotNullable()
			->setDataType(Column::DATATYPE_DATE);
		$table->getColumn(self::FIELD_transaktion_bank)
			->setNotNullable()
			->setDataTypeInteger()
			->addForeignKey($table, TransaktionBank::table_name);
		$table->getColumn(self::FIELD_betrag)
			->setGuiName(Trans::late("Amount"))
			->setNotNullable()
			->setDataTypeInteger();
		$table->getColumn(self::FIELD_verwendungszweck)
			->setGuiName(Trans::late("Purpose of use"))
			->setDataType(Column::DATATYPE_TEXT);
		$table->getColumn(self::FIELD_buchungskonto)
			->setGuiName(Trans::late(KontoBuchung::gui_name))
			->setDataTypeInteger()
			->addForeignKey($table, KontoBuchung::$table_name);
		$table->getColumn(self::FIELD_gegenbuchung)
			->setGuiName(Trans::late("Offsetting entry"))//"Gegenbuchung"
			->setDataTypeInteger()
			->addForeignKey($table, TransaktionBuchung::table_name);
		$table->getColumn(self::FIELD_await)
			->setGuiName("Gegenbuchung erwartet")
			->setDataType(Column::DATATYPE_BOOLEAN)
			->setNotNullable()
			->setDefault("'0'");
	}

	public static function fromImport($datum, $betrag, $text, $transaktion_bank, $buchungskonto)
	{

		if (!$buchungskonto || $buchungskonto === Form::TT_NULL_VALUE) $buchungskonto = null;

		$transaction = new TransaktionBuchung();
		$transaction->setDataArray(array(
			self::FIELD_datum => $datum,
			self::FIELD_betrag => $betrag,
			self::FIELD_verwendungszweck => $text,
			self::FIELD_transaktion_bank => $transaktion_bank,
			self::FIELD_buchungskonto => $buchungskonto,
		));

		return $transaction;
	}

	/**
	 * @return int
	 */
	public function getBetrag()
	{
		return $this->betrag;
	}

	/**
	 * @return string
	 */
	public function getDatum()
	{
		return $this->datum;
	}

	/**
	 * @return int
	 */
	public function getGegenbuchung()
	{
		return $this->gegenbuchung;
	}

	/**
	 * @return int
	 */
	public function getBuchungskonto()
	{
		return $this->buchungskonto;
	}

	/**
	 * @return TransaktionBank
	 * @noinspection PhpUnused
	 */
	public function getTransaktionBank()
	{
		return $this->transaktion_bank;
	}

	/**
	 * @return string
	 * @noinspection PhpUnused
	 */
	public function getVerwendungszweck()
	{
		return $this->verwendungszweck;
	}

	/**
	 * @param int $gegenbuchung
	 * @return TransaktionBuchung
	 */
	public function setGegenbuchung($gegenbuchung, $persistCounterpart = true)
	{
		$this->gegenbuchung = $gegenbuchung;
		if (!$gegenbuchung) return $this;

		$this->await = false;
		$this->buchungskonto = null;
		if (!$persistCounterpart) return $this;

		/** @noinspection PhpUnusedLocalVariableInspection
		 * Class member access on instantiation is only allowed since PHP 5.4 */
		($x = new TransaktionBuchung())
			->fromDbWhereEqualsId($gegenbuchung)
			->setGegenbuchung($this->getId(), false)
			->persist();
		return $this;
	}

	/**
	 * @param int $buchungskonto
	 * @return TransaktionBuchung
	 */
	public function setBuchungskonto($buchungskonto)
	{
		$this->buchungskonto = $buchungskonto;
		return $this;
	}

	private function editFormPostProcessSplit($betrag_before)
	{
		if ($this->betrag == $betrag_before) return;
		$this->gegenbuchung = $this->gegenbuchung ? self::GEGENBUCHUNG_AWAIT : null;

		$split_betrag = $betrag_before - $this->betrag;
		$split_datum = ServiceEnv::valueFromPost(self::POSTVAL_SPLIT_DATUM);
		$split_text = ServiceEnv::valueFromPost(self::POSTVAL_SPLIT_VERWENDUNGSZWECK);
		$split_kat = ServiceEnv::valueFromPost(self::POSTVAL_SPLIT_BUCHUNGSKONTO);

		$buchung = new TransaktionBuchung();
		$buchung->setDataFromReadable(self::FIELD_datum, $split_datum);
		if ($split_kat === self::SPECIAL_GEGENBUCHUNG) {
			$buchung->await = true;
		} else {
			$buchung->setDataFromReadable(self::FIELD_buchungskonto, $split_kat);
		}
		$buchung->setDataArray(array(
			self::FIELD_transaktion_bank => $this->transaktion_bank,
			self::FIELD_verwendungszweck => $split_text,
			self::FIELD_betrag => $split_betrag,
		));

		$buchung->persistStrict();
	}

	public function editFormPostProcess()
	{
		$gegenbuchung_before = $this->gegenbuchung;
		$betrag_before = $this->betrag;
		$this->fromPost();

		$this->editFormPostProcessSplit($betrag_before);

		if ($this->gegenbuchung === self::GEGENBUCHUNG_AWAIT) {
			$this->resetGegenbuchung(false);
		} else {
			$this->setGegenbuchung($this->gegenbuchung);
			if (!$this->gegenbuchung) $this->await = false;
		}

		//If the Gegenbuchung has changed, reset the one before
		if ($this->gegenbuchung != $gegenbuchung_before && $gegenbuchung_before) {
			(new TransaktionBuchung())->fromDbWhereEqualsId($gegenbuchung_before)->resetGegenbuchung();
		}

		$ok = $this->persistStrict();
		if($ok===false){
			new MsgError("Failed!");
		}else{
			new MsgConfirm("Success.");
		}
		return null;
	}

	function getGuiName()
	{
		return Trans::late(self::gui_name);
	}

	public function getEditFormJs()
	{
		return array(
			ConfigServer::$HTTP_TT_ROOT . "/features/javascripts/financial.js",
			ConfigServer::$HTTP_MODULE_ROOT[ModuleGiro::$module_id] . "/v2/giro.js",
		);
	}

	/**
	 * @param Column $column
	 * @param ForeignKey|null $foreignKey
	 * @return HtmlCompFormInput|null|HtmlCompFormInput[]
	 */
	public function getEditFormField(Column $column, $foreignKey)
	{
		$colname = $column->getName();

		if ($colname === self::FIELD_transaktion_bank) {
			$transaktionBank = new TransaktionBank();
			$transaktionBank->fromDbWhereEqualsId($this->getTransaktionBank());
			return new FormDiv(Trans::late(TransaktionBank::gui_name), $transaktionBank->toHtml());
		}

		if ($colname === self::FIELD_betrag) {
			$field = parent::getEditFormField($column, $foreignKey);
			$field->addKeyVal(HtmlComponent::KEY_ONKEYUP, "ttgiro.splitUpdate(this.value," . $this->getBetrag() . ");");
			return $field;
		}

		if ($colname === self::FIELD_verwendungszweck) {
			$field = parent::getEditFormField($column, $foreignKey);
			if($field instanceof FormInputTextarea){
				$field->addFunction(new FunctionEmoji(Php7::mb_chr(UnicodeIcons::Heavy_Check_Mark)));
			}
			return $field;
		}

		if ($colname === self::FIELD_gegenbuchung) {
			$options = array(
				Form::TT_NULL_VALUE => "(no)",
				self::GEGENBUCHUNG_AWAIT => ">>> ".Trans::late("EXPECTED/PENDING/AWAITING")." <<<",
			);
			$options += $this->getAllGegenbuchungen();
			if ($this->gegenbuchung !== null) {
				($gegenbuchung=new TransaktionBuchung())->fromDbWhereEqualsId($this->gegenbuchung);
				($transaktionBank=new TransaktionBank())->fromDbWhereEqualsId($gegenbuchung->getTransaktionBank());
				($konto = new KontoBank())->fromDbWhereEqualsId($transaktionBank->getKonto());
				$options += array($this->gegenbuchung => self::gegenbuchungEntry(
					$this->gegenbuchung,
					$konto->getName(),
					$gegenbuchung->getDatum(),
					$gegenbuchung->getVerwendungszweck() ?: $transaktionBank->getVerwendungszweck()
				));
			}
			$value = ($this->await ? self::GEGENBUCHUNG_AWAIT : $this->getGegenbuchung());
			$dropdown = new FormInputDropdown($colname, $options, $column->getGuiName(), $value);
			$dropdown->setId('giro_gegenbuchung_id');
			return $dropdown;
		}

		if ($colname === self::FIELD_await) {
			return null;
		}

		if ($colname === self::FIELD_buchungskonto) {
			$split = new FormSubset("Split");
			$split->setId('tt_splitsub_id');
			$split->addClass(HtmlComponent::CLASS_HIDE);
			$split->add(new FormInputText(self::POSTVAL_SPLIT_DATUM,
				$this->getGuiNameOfColumn(self::FIELD_datum),
				date("d.m.Y", strtotime($this->getDatum()))
			));
			$split->add($splitBetrag = new FormInputText("x",
				$this->getGuiNameOfColumn(self::FIELD_betrag)
			));
			$splitBetrag->setReadonly();
			$splitBetrag->setId("tt_split_betrag_id");
			$split->add(new FormInputTextarea(self::POSTVAL_SPLIT_VERWENDUNGSZWECK,
				$this->getGuiNameOfColumn(self::FIELD_verwendungszweck),
				$this->getVerwendungszweck()
			));

			$options = TransaktionBuchung::getAlleBuchungskonten(true);
			$split->add(new FormInputDropdown(self::POSTVAL_SPLIT_BUCHUNGSKONTO,
				$options,
				$this->getGuiNameOfColumn(self::FIELD_buchungskonto),
				$this->getBuchungskonto()
			));

			$options = TransaktionBuchung::getAlleBuchungskonten();
			$options = array(Form::TT_NULL_VALUE => "(unknown)") + $options;
			$f=(new FormInputDropdown(self::FIELD_buchungskonto,
				$options,
				$this->getGuiNameOfColumn(self::FIELD_buchungskonto),
				$this->getBuchungskonto()
			));

			return array($f, $split);
		}

		return parent::getEditFormField($column, $foreignKey);
	}

	public function getDataToReadable($key)
	{
		if ($key === self::FIELD_betrag) {
			return ServiceFinancial::centsToEuro($this->betrag, true, "");
		}
		return parent::getDataToReadable($key);
	}

	public function setDataFromReadable($key, $value)
	{
		if ($key === self::FIELD_betrag) {
			$this->betrag = ServiceFinancial::euroToCents($value);
		} else {
			parent::setDataFromReadable($key, $value);
		}
	}

	/**
	 * @return string[]
	 */
	private function getAllGegenbuchungen()
	{
		$betrag = $this->getBetrag();
		$db = DatabaseHandler::getDefaultDb();
		$query_gegenbuchungen = $db->generalQuery(
			array(
				Model::FIELD_id,
				self::FIELD_datum,
				self::FIELD_verwendungszweck,
				KontoBank::$table_name . '.' . KontoBank::FIELD_name,
				TransaktionBank::table_name . '.' . TransaktionBank::FIELD_verwendungszweck . " as v2"
			),
			self::table_name,
			array(
				new WhereEquals(self::FIELD_await, true),
				new WhereCondition(
					self::table_name . "." . Model::FIELD_id,
					WhereCondition::RELATION_NOT_EQUALS,
					$this->getId()),
				new WhereEquals(self::table_name . "." . self::FIELD_betrag, -$betrag),
				new LeftJoinStatement(TransaktionBank::table_name, self::FIELD_transaktion_bank),
				new LeftJoinStatement(
					KontoBank::$table_name,
					TransaktionBank::table_name . '.' . TransaktionBank::FIELD_konto),
			)
		);
		$result = array();
		foreach ($query_gegenbuchungen as $row) {
			$id = $row[Model::FIELD_id];
			$result[$id] = self::gegenbuchungEntry(
				$id,
				$row[KontoBank::FIELD_name],
				$row[self::FIELD_datum],
				$row[self::FIELD_verwendungszweck] ?: $row["v2"]
			);
		}
		return $result;
	}

	public static function gegenbuchungEntry($id, $konto, $datum, $verwendungszweck, $maxlength = 80)
	{
		return date("d.m.", strtotime($datum))
			. " " . htmlentities(ServiceStrings::cutAndEllipsis($verwendungszweck, $maxlength) ?: "")
			. " (" . $konto . ")"
			. (CFG_S::$DEVMODE ? " (#$id)" : "");
	}

	/**
	 * await = TRUE, Gegenbuchung = NULL
	 * @param bool $persist
	 * @return void
	 */
	private function resetGegenbuchung($persist = true)
	{
		$this->await = true;
		$this->gegenbuchung = null;
		if ($persist) $this->persist();
	}

}