<?php /** @noinspection PhpUnused TODO */

namespace ttgiro\v2\model;

use tt\features\database\schema\Table;
use tt\features\database\v1\Model;
use tt\features\i18n\Trans;

class AbschluesseBank extends Model
{

	const table_name = "giro_abschluesse";
	protected $tableName = self::table_name;
	const gui_name = "Bank balance";//(monthly)

	protected $bankkonto;
	const FIELD_bankkonto = 'bankkonto';
	protected $monat;
	const FIELD_monat = 'monat';
	protected $saldo;
	const FIELD_saldo = 'saldo';

	public static function fromDbByMonat($month, $konto_id)
	{
		$abschluss = new self();
		$ok = $abschluss->fromDbWhereEquals(array(
			self::FIELD_monat => $month,
			self::FIELD_bankkonto => $konto_id,
		));
		if (!$ok) return false;
		return $abschluss;
	}

	function getGuiName()
	{
		return Trans::late(self::gui_name);
	}

	/**
	 * @param Table $table
	 * @return void
	 */
	function changeDefaultSchema(Table $table)
	{
		$table->getColumn(self::FIELD_bankkonto)
			->setNotNullable()
			->setDataTypeInteger()
			->addForeignKey($table, KontoBank::$table_name);
		$table->getColumn(self::FIELD_monat)
			->setNotNullable();
		$table->getColumn(self::FIELD_saldo)
			->setNotNullable()
			->setDataTypeInteger();
		$table->addIndexUnique(array(
			$table->getColumn(self::FIELD_bankkonto),
			$table->getColumn(self::FIELD_monat),
		));
	}

	/**
	 * @return int
	 */
	public function getSaldo()
	{
		return $this->saldo;
	}

}