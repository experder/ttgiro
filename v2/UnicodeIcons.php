<?php

namespace ttgiro\v2;

class UnicodeIcons
{

	//1f300-1f5ff Miscellaneous Symbols and Pictographs
	const Bank = 0x1f3e6;//🏦

	const money_with_wings = 0x1F4B8;//💸
	const receipt = 0x1F9FE;//🧾
	const seal=0x1F9AD;//🦭

}