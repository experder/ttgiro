<?php

namespace ttgiro\v2\features\import;

use tt\features\debug\errorhandler\v1\Error;
use tt\services\ServiceFiles;
use tt\services\ServiceFinancial;
use ttgiro\v2\model\DkbHashLog;
use ttgiro\v2\model\TransaktionBank;

class Import_DKB extends Import
{

	function setLatestTransactions()
	{
		$content = ServiceFiles::get_contents($this->file, false);
		$content_array = explode("\n", $content);

		//Plausi: Kontostand (Zeile 5):
		if(!preg_match("/^\"Kontostand vom (.*?):\";\"(.*?) EUR\";\$/", $content_array[4], $matches)){
			new Error("DKB format has changed or file is corrupt. Kontostand.");
		}
		$this->latest_balance = ServiceFinancial::euroToCents($matches[2]);

		//Plausi: Zeile [offset] enthält Spaltenbeschriftungen:
		$offset = 7;
		if ($content_array[$offset - 1] !== '"Buchungstag";"Wertstellung";"Buchungstext";"Auftraggeber / Begünstigter";'
			. '"Verwendungszweck";"Kontonummer";"BLZ";"Betrag (EUR)";"Gläubiger-ID";"Mandatsreferenz";'
			. '"Kundenreferenz";') {
			new Error("DKB format has changed or file is corrupt. Header.");
		}

		$transaction_array = array();
		for ($i = $offset; $i < count($content_array); $i++) {
			$row = $content_array[$i];

			//Plausi: CSV-Format "";... mit 11 Spalten:
			if (!preg_match("/^"
				. "\"(?<Buchungstag>.*?)\";"
				. "\"(?<Wertstellung>.*?)\";"
				. "\"(?<Buchungstext>.*?)\";"
				. "\"(?<auftraggeber>.*?)\";"
				. "\"(?<Verwendungszweck>.*?)\";"
				. "\"(?<Kontonummer>.*?)\";"
				. "\"(?<BLZ>.*?)\";"
				. "\"(?<betrag>.*?)\";"
				. "\"(?<glaeubiger_id>.*?)\";"
				. "\"(?<Mandatsreferenz>.*?)\";"
				. "\"(?<Kundenreferenz>.*?)\";"
				. "\$/", $row, $matches)) new Error("DKB format has changed or file is corrupt. Line " . ($i + 1));

			//Plausi: Betrag: -1.000,00
			$betrag = $matches['betrag'];
			if (!preg_match("/^-?[\\d.]*,\\d{2}\$/", $betrag)) {
				new Error("Invalid amount: $betrag");
			}
			$betrag = str_replace(array(".", ","), array("", "."), $betrag) * 100;

			//Plausi: Datum: 01.01.1979
			$datum = $matches['Buchungstag'];
			if (!preg_match("/^\\d{2}\\.\\d{2}\\.\\d{4}\$/", $datum)) new Error("Invalid date '$datum'!");
			$datum = date("Y-m-d", strtotime($datum));

			$text = $matches['Buchungstext']
				. "\n" . $matches['auftraggeber']
				. "\n" . $matches['Verwendungszweck']
				. "\n" . $matches['Kontonummer'];

			$transaction = TransaktionBank::fromImport($datum, $betrag, $text);

			$transaction_array[] = $transaction;
		}

		$this->transactions=$transaction_array;
	}

	public function transactionPostprocess(TransaktionBank $transaktion)
	{
		DkbHashLog::fromTransaction($transaktion)->persist();
	}

	/**
	 * @return string
	 */
	function getImportHandlerId()
	{
		return "dkb";
	}

	/**
	 * @return string
	 */
	function getImportHandlerDesc()
	{
		return "DKB";
	}
}