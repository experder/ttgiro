<?php

namespace ttgiro\v2\features\import;

use tt\features\debug\errorhandler\v1\Error;
use tt\services\ServiceFiles;
use tt\services\ServiceFinancial;
use ttgiro\v2\model\DkbHashLog;
use ttgiro\v2\model\TransaktionBank;

class Import_DKBneu24 extends Import
{

    function setLatestTransactions()
    {
        $content = ServiceFiles::get_contents($this->file);
        #echo "<pre>".print_r($content)."</pre>";exit;
        $content_array = explode("\n", $content);

        //Plausi: Kontostand (Zeile 3):
        if (!preg_match("/^\"Kontostand vom ([\\d.]*?):\";\"([\\d.,]*?) €\"\$/", $content_array[2], $matches)) {
            new Error("DKB format has changed or file is corrupt. Kontostand.");
        }
        $this->latest_balance = ServiceFinancial::euroToCents($matches[2]);

        //Plausi: Zeile [offset] enthält Spaltenbeschriftungen:
        $offset = 5;
        if ($content_array[$offset - 1] !== '"Buchungsdatum";"Wertstellung";"Status";"Zahlungspflichtige*r";'
            . '"Zahlungsempfänger*in";"Verwendungszweck";"Umsatztyp";"IBAN";"Betrag (€)";"Gläubiger-ID";'
            . '"Mandatsreferenz";"Kundenreferenz"') {
            new Error("DKB format has changed or file is corrupt. Header.");
        }

        $transaction_array = array();
        for ($i = $offset; $i < count($content_array); $i++) {
            $row = $content_array[$i];

            //Plausi: CSV-Format "";... mit 11 Spalten:
            if (!preg_match("/^"
                . "\"(?<Buchungsdatum>.*?)\";"
                . "\"(?<Wertstellung>.*?)\";"
                . "\"(?<Status>.*?)\";"
                . "\"(?<zahlungspflichtige>.*?)\";"
                . "\"(?<zahlungsempfaenger>.*?)\";"
                . "\"(?<Verwendungszweck>.*?)\";"
                . "\"(?<Umsatztyp>.*?)\";"
                . "\"(?<IBAN>.*?)\";"
                . "\"(?<betrag>.*?)\";"
                . "\"(?<glaeubiger>.*?)\";"
                . "\"(?<Mandatsreferenz>.*?)\";"
                . "\"(?<Kundenreferenz>.*?)\"\$/",
                $row, $matches)) new Error("DKB format has changed or file is corrupt. Line " . ($i + 1));

            //Plausi: Betrag: -1.000,00
            $betrag = $matches['betrag'];
            if (!preg_match("/^-?[\\d.]*,?\\d{1,2}\$/", $betrag)) {
                new Error("Invalid amount: $betrag");
            }
            $betrag = str_replace(array(".", ","), array("", "."), $betrag) * 100;

            //Plausi: Datum: 01.01.79
            $datum = $matches['Buchungsdatum'];
            if (!preg_match("/^(?<d>\\d{2})\\.(?<m>\\d{2})\\.(?<y>\\d{2})\$/", $datum, $datumMatches)) new Error("Invalid date '$datum'!");
            $datum = "20" . $datumMatches["y"] . "-" . $datumMatches["m"] . "-" . $datumMatches["d"];

            $text = ($betrag > 0 ? $matches['zahlungspflichtige'] : $matches['zahlungsempfaenger'])
                . "\n" . $matches['Verwendungszweck']
                . "\n" . $matches['IBAN'];

            if ($matches['Status'] === "Vorgemerkt") continue;
            $transaction_array[] = TransaktionBank::fromImport($datum, $betrag, $text);
        }

        $this->transactions = $transaction_array;
    }

    public function transactionPostprocess(TransaktionBank $transaktion)
    {
        DkbHashLog::fromTransaction($transaktion)->persist();
    }

    /**
     * @return string
     */
    function getImportHandlerId()
    {
        return "dkb24";
    }

    /**
     * @return string
     */
    function getImportHandlerDesc()
    {
        return "DKB 24";
    }
}