<?php

namespace ttgiro\v2\features\import;

use tt\features\database\v1\LimitClause;
use tt\features\database\v1\Model;
use tt\features\database\v1\OrderClause;
use tt\features\database\v1\WhereEquals;
use tt\features\htmlpage\components\FormInputText;
use tt\features\htmlpage\components\HtmlDiv;
use tt\features\i18n\Trans;
use tt\features\thirdparty\v1\assets\Chosen1;
use tt\features\thirdparty\v1\ThirdpartyHandler;
use ttgiro\v2\model\TransaktionBank;

class Import_Manuell extends Import
{

	function setLatestTransactions()
	{
	}

	public function getPreviewHtml()
	{
		ThirdpartyHandler::addGlobalAsset(new Chosen1());
		$form = $this->newTransactionsFormInitial(false);
		/** @noinspection PhpUnusedLocalVariableInspection */
		$form->add(($seed=new HtmlDiv())->setId('manual_import_seed_div'));
		$form->add(new FormInputText(ViewImport::POSTVAL_EXECUTE_BALANCE_CHECK));
		return $this->htmlLastTransaction()
			.$form->toHtml();
	}

	private function htmlLastTransaction(){
		$transaction = new TransaktionBank();
		$transaction->fromDbWhere(array(
			new WhereEquals(TransaktionBank::FIELD_konto,$this->konto_id),
			new OrderClause(TransaktionBank::FIELD_datum, OrderClause::DESC),
			new OrderClause(Model::FIELD_id, OrderClause::DESC),
			new LimitClause(1),
		));
		$transaction_alt = new TransaktionBank();
		$transaction_alt->fromDbWhere(array(
			new WhereEquals(TransaktionBank::FIELD_konto,$this->konto_id),
			new OrderClause(TransaktionBank::FIELD_datum, OrderClause::DESC),
			new OrderClause(Model::FIELD_id),
			new LimitClause(1),
		));
		return "Last transaction: ".$transaction->toHtml()
			.($transaction->getId()!=$transaction_alt->getId()
			?"<div class='alternativeLastTransaction'>".$transaction_alt->toHtml()."</div>"
			:"")
			;
	}

	/**
	 * @return string
	 */
	function getImportHandlerId()
	{
		return "manuell";
	}

	/**
	 * @return string
	 */
	function getImportHandlerDesc()
	{
		return Trans::late("Manual");
	}
}