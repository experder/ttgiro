<?php

namespace ttgiro\v2\features\import;

use tt\features\debug\errorhandler\v1\Error;
use tt\services\ServiceEnv;

class ServiceImport
{

	/**
	 * @var Import[] $allImportHandlers
	 */
	private static $allImportHandlers = null;

	public static function getAllImportHandlers()
	{
		if (self::$allImportHandlers === null) {
			self::$allImportHandlers = array(
				new Import_Manuell(),
				#new Import_DKB(),
                new Import_DKBneu24(),
				new Import_VW(),
			);
		}
		return self::$allImportHandlers;
	}

	/**
	 * @param Import[]|true $handlerArray
	 * @return string[]
	 */
	public static function idDescFromHandlerArray($handlerArray = true)
	{
		if ($handlerArray === true) $handlerArray = self::getAllImportHandlers();
		$options = array();
		foreach ($handlerArray as $importHandler) {
			$options[$importHandler->getImportHandlerId()] = $importHandler->getImportHandlerDesc();
		}
		return $options;
	}

	public static function getHandlerFromPost()
	{
		$handler_id = ServiceEnv::valueFromPost(ViewImport::POSTVAL_FORMAT);
		$handler = self::getHandlerById($handler_id);
		if ($handler === false) {
			new Error("Unknown import handler '$handler_id'!");
		}
		return $handler;
	}

	/**
	 * @param string $handler_id
	 * @return Import|false
	 */
	public static function getHandlerById($handler_id)
	{
		foreach (self::getAllImportHandlers() as $handler) {
			if ($handler->getImportHandlerId() === $handler_id) {
				return $handler;
			}
		}
		return false;
	}

}