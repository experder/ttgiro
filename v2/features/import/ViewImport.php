<?php

namespace ttgiro\v2\features\import;

use tt\features\config\v1\CFG_S;
use tt\features\config\v1\ConfigProject;
use tt\features\config\v1\ConfigServer;
use tt\features\css\Stylesheet;
use tt\features\database\v1\DatabaseHandler;
use tt\features\database\v1\db_mysql\DatabaseMySql;
use tt\features\database\v1\Model;
use tt\features\database\v1\OrderClause;
use tt\features\database\v1\WhereEquals;
use tt\features\debug\errorhandler\v1\Error;
use tt\features\debug\errorhandler\v2\Warning;
use tt\features\frontcontroller\bycode\ServiceRoutes;
use tt\features\htmlpage\components\Form;
use tt\features\htmlpage\components\FormInputCheckbox;
use tt\features\htmlpage\components\FormInputDropdown;
use tt\features\htmlpage\components\FormInputText;
use tt\features\htmlpage\components\HtmlComponent;
use tt\features\htmlpage\view\v1\ViewHtmlNew;
use tt\features\i18n\Trans;
use tt\features\javascripts\ServiceJs;
use tt\features\messages\v2\MsgConfirm;
use tt\services\polyfill\Php7;
use tt\services\ServiceArrays;
use tt\services\ServiceDateTime;
use tt\services\ServiceEnv;
use tt\services\ServiceFiles;
use tt\services\ServiceFinancial;
use tt\services\UnicodeIcons;
use ttgiro\v2\model\AbschluesseBank;
use ttgiro\v2\model\KontoBank;
use ttgiro\v2\model\schema\AbschluesseBank2;
use ttgiro\v2\model\TransaktionBank;
use ttgiro\v2\model\TransaktionBuchung;
use ttgiro\v2\ModuleGiro;
use ttgiro\v2\views\ViewBankAccount;

class ViewImport extends ViewHtmlNew
{

	const POSTVAL_FORMAT = "format";
	const POSTVAL_KONTO = "konto";

	const CMD_PREVIEW = "preview";
	const POSTVAL_PREVIEW_FILE = "file";
	const POSTVAL_PREVIEW_DELETE = "delete";
	const POSTVAL_PREVIEW_SAVE = "save";

	const CMD_EXECUTE = "execute";
	const POSTVAL_EXECUTE_BETRAG = "betrag";
	const POSTVAL_EXECUTE_DATUM = "datum";
	const POSTVAL_EXECUTE_KATEGORIE = "kategorie";
	const POSTVAL_EXECUTE_VERWENDUNGSZWECK = "verwendungszweck";
	const POSTVAL_EXECUTE_DELETEFILE = "deletefile";
	const POSTVAL_EXECUTE_OVERRIDE = "override";
	const POSTVAL_EXECUTE_LAST_BALANCE = "balance";
	const POSTVAL_EXECUTE_BALANCE_CHECK = "balancecheck";

	private $cmd;
	/**
	 * @var Import $handler
	 */
	private $handler = null;

	private $konto_id;

	public function __construct()
	{
		$this->cmd = ServiceEnv::valueFromPost(Form::COMMAND_STRING);
		if($this->cmd==ViewImport::CMD_PREVIEW || $this->cmd==ViewImport::CMD_EXECUTE){
			$this->handler = ServiceImport::getHandlerFromPost();
		}
		$this->konto_id = ServiceEnv::valueFromPost(ViewImport::POSTVAL_KONTO);
	}

	public static function getClass()
	{
		/** @noinspection PhpFullyQualifiedNameUsageInspection */
		return \tt\services\polyfill\Php5::get_class();
	}

	/**
	 * @return string
	 */
	public function getHtml()
	{
		if (!DatabaseHandler::checkDbDependencies()) return "";
		switch ($this->cmd) {
			case ViewImport::CMD_PREVIEW:
				return $this->htmlPreview();
			case ViewImport::CMD_EXECUTE:
				return $this->htmlExecute();
			default:
				if ($this->cmd) new Error("Unknown command!");
				return $this->htmlInitial();
		}
	}

	/**
	 * @return string
	 */
	public function getTitle()
	{
		if($this->cmd==ViewImport::CMD_PREVIEW || $this->cmd==ViewImport::CMD_EXECUTE){
			($konto=new KontoBank())->fromDbWhereEqualsId(ServiceEnv::valueFromPost(self::POSTVAL_KONTO));
			return self::title()." - ".$konto->getName();
		}
		return self::title();
	}

	public static function title()
	{
		return Php7::mb_chr(UnicodeIcons::Magic_Wand)." ".Trans::late("Import");
	}

	private function previewSaveDefault()
	{
		if (!ServiceEnv::valueFromPost(self::POSTVAL_PREVIEW_SAVE)) {
			return;
		}

		($konto = new KontoBank())->fromDbWhereEqualsId(ServiceEnv::valueFromPost(self::POSTVAL_KONTO));
		if (!$konto->getId()) {
			new Warning("Could not store settings!");
			return;
		}

		$konto->setDataArray(array(
			KontoBank::FIELD_import_file => ServiceEnv::valueFromPost(self::POSTVAL_PREVIEW_FILE),
			KontoBank::FIELD_import_format => ServiceEnv::valueFromPost(self::POSTVAL_FORMAT),
			KontoBank::FIELD_import_delete => ServiceEnv::valueFromPost(self::POSTVAL_PREVIEW_DELETE) ? 1 : 0,
		));
		$konto->persistStrict();

		new MsgConfirm(Trans::late("Settings saved") . ".");
	}

	private function htmlPreview()
	{
		$this->previewSaveDefault();

		$this->handler->fromImportPost();

		$this->handler->setLatestTransactions();

		return $this->handler->getPreviewHtml();
	}

	private function htmlInitial()
	{
		$form = new Form(ViewImport::CMD_PREVIEW, Trans::late("Import!"));

		$result = DatabaseHandler::getDefaultDb()->generalQuery(
			array(Model::FIELD_id, KontoBank::FIELD_name),
			KontoBank::$table_name,
			array(
				new OrderClause(KontoBank::FIELD_orderby, false),
				new OrderClause(KontoBank::FIELD_name),
			)
		);
		$options = ServiceArrays::keyValueArray($result, "{" . KontoBank::FIELD_name . "}"
			. (CFG_S::$DEVMODE ? " ({" . Model::FIELD_id . "})" : "")
		);
		$form->add($konto_selector
			= new FormInputDropdown(ViewImport::POSTVAL_KONTO, $options, Trans::late(KontoBank::gui_name)));
		$konto_selector->addKeyVal(HtmlComponent::KEY_ONCHANGE, "ttgiro.setImportForm(this.value);");
		$konto_selector->setId('id_kontoSelector');

		$options = ServiceImport::idDescFromHandlerArray();
		$form->add($format
			= new FormInputDropdown(ViewImport::POSTVAL_FORMAT, $options, Trans::late(Import::GUINAME_FORMAT)));
		$format->setId('id_format');

		$form->add($datei = new FormInputText(ViewImport::POSTVAL_PREVIEW_FILE, Trans::late("File")));
		$datei->setHelp("https://gitlab.com/experder/tt2/-/blob/main/features/templates/tt_template/ServiceTtTemplate.md");
		$datei->setId('id_file');

		$form->add($delete = new FormInputCheckbox(ViewImport::POSTVAL_PREVIEW_DELETE, Trans::late("Delete file")));
		$delete->setId('id_delete');

		$form->add(new FormInputCheckbox(ViewImport::POSTVAL_PREVIEW_SAVE, Trans::late("Save settings")));

		return $form->toHtml();
	}

	public function getHeadJs()
	{
		if($this->cmd===ViewImport::CMD_PREVIEW && ($this->handler instanceof Import_Manuell)){
			return ServiceJs::onLoad(
				'ttgiro.newTransaction(document.getElementById("manual_import_seed_div").parentNode,1);');
		}
		if ($this->cmd) return "";
		//Initial:
		return ServiceJs::onLoad('let a=document.getElementById("id_kontoSelector").value;if(a)ttgiro.setImportForm(a);');
	}

	private function htmlExecute()
	{
		$konto_id = ServiceEnv::valueFromPost(ViewImport::POSTVAL_KONTO, false);
		$transacions = array();
		$number = 0;
		/** @noinspection PhpStatementHasEmptyBodyInspection */
		while (isset($_POST[ViewImport::POSTVAL_EXECUTE_DATUM . (++$number)])) ;
		$number--;
		for ($counter = $number; $counter >= 1; $counter--) {
			$betrag = ServiceFinancial::euroToCents($_POST[ViewImport::POSTVAL_EXECUTE_BETRAG . $counter]);
			$verwendungszweck = $_POST[ViewImport::POSTVAL_EXECUTE_VERWENDUNGSZWECK . $counter];
			if($this->handler instanceof Import_Manuell && !$betrag && !$verwendungszweck){
				$number--;
				continue;
			}
			$datum = date("Y-m-d", strtotime($_POST[ViewImport::POSTVAL_EXECUTE_DATUM . $counter]));
			$kategorie = $_POST[ViewImport::POSTVAL_EXECUTE_KATEGORIE . $counter];

			$gegenbuchung = false;
			if ($kategorie === TransaktionBuchung::SPECIAL_GEGENBUCHUNG) {
				$kategorie = null;
				$gegenbuchung = true;
			}

			$transaction = TransaktionBank::fromImport($datum, $betrag, $verwendungszweck, $konto_id);
			if(!($this->handler instanceof Import_Manuell)) $transaction->setHash();

			if (isset($_POST[ViewImport::POSTVAL_EXECUTE_OVERRIDE . $counter])) {
				$override_id = $_POST[ViewImport::POSTVAL_EXECUTE_OVERRIDE . $counter];
				$this->updateSimilarTransaction($transaction, $override_id);
				$transaction->setId($override_id);
			} else {
				$transaction->persistStrict();
				$buchung = TransaktionBuchung::fromImport($datum, $betrag, null, $transaction->getId(), $kategorie);
				if ($gegenbuchung) $buchung->setAwait();
				$buchung->persistStrict();
			}

			$this->handler->transactionPostprocess($transaction);
			$transacions[] = $transaction;
		}

		new MsgConfirm("Imported " . ($number) . " lines.");

		$deletefile = ServiceEnv::valueFromPost(ViewImport::POSTVAL_EXECUTE_DELETEFILE, false);
		if ($deletefile) {
			if (ServiceFiles::unlink_file($deletefile)) {
				new MsgConfirm("Deleted file '" . basename($deletefile) . "'.");
			}
		}

		$ok = $this->checkBalance();

		if($ok)$this->confirmBalance($transacions);

		return "[ ".ServiceRoutes::getLinkComponent(ViewBankAccount::getClass(), true, array(
					ViewBankAccount::GETVAL_konto=>$konto_id,
				))." ]";
	}

	/**
	 * @param TransaktionBank[] $transactions
	 * @return void
	 */
	private function confirmBalance(array $transactions) {
		if(!$transactions)return;

		$datum_last = "0";
		foreach ($transactions as $transaktionBank){
			$datum = $transaktionBank->getDatum();
			if($datum>$datum_last)$datum_last=$datum;
		}

		$monatLetzterImport = date("Y-m", strtotime($datum_last));
		$abschluss_monat = ServiceDateTime::vormonat($monatLetzterImport);

		$latest_abschluss = AbschluesseBank2::latestAbschluss($this->konto_id);
		$monatNachLetztemAbschluss = ServiceDateTime::folgemonat($latest_abschluss[AbschluesseBank2::COL_monat]);

		if($monatLetzterImport<=$monatNachLetztemAbschluss){
			return;
		}

		$latest_abschluss_saldo = $latest_abschluss[AbschluesseBank2::COL_saldo];
		$summe_seitdem = 0;
		foreach (ViewBankAccount::queryAllTransactions(
			$this->konto_id, $monatNachLetztemAbschluss."-01", $monatLetzterImport."-01"
		) as $row) {
			$summe_seitdem += $row[TransaktionBank::FIELD_betrag];
		}
		$abschluss_wert = $latest_abschluss_saldo+$summe_seitdem;

		$abschluss = new AbschluesseBank();
		$abschluss->setDataArray(array(
			AbschluesseBank::FIELD_bankkonto => $this->konto_id,
			AbschluesseBank::FIELD_monat => $abschluss_monat,
			AbschluesseBank::FIELD_saldo => $abschluss_wert,
		));
		$abschluss->persistStrict();

		new MsgConfirm("Abschluss für $abschluss_monat: ".ServiceFinancial::centsToEuro($abschluss_wert));
	}

	private function checkBalance() {
		$balance_quoted=ServiceEnv::valueFromPost(ViewImport::POSTVAL_EXECUTE_BALANCE_CHECK);
		if($balance_quoted!==null&&$balance_quoted!==''){
			$balance_quoted=ServiceFinancial::euroToCents($balance_quoted);
		}else{
			$balance_quoted=ServiceEnv::valueFromPost(ViewImport::POSTVAL_EXECUTE_LAST_BALANCE);
			if($balance_quoted===null)return false;
		}

		$konto_id = $this->konto_id;
		$latest_abschluss = AbschluesseBank2::latestAbschluss($konto_id);
		if(!$latest_abschluss){
			new MsgConfirm("Balance check impossible (no Abschluss yet).");
			return false;
		}
		$seit = ServiceDateTime::folgemonat($latest_abschluss[AbschluesseBank2::COL_monat]."-02")."-01";
		$db = DatabaseHandler::getDefaultDb();
		if($db instanceof DatabaseMySql)
		$sum_after = $db->select("SELECT sum(betrag) as summe"
			." FROM giro_transaktion_konto where konto=:konto and datum >= :datum",array(
			":konto"=>$konto_id,
			":datum"=>$seit,
		));
		$balance = $latest_abschluss[AbschluesseBank2::COL_saldo]+$sum_after[0]['summe'];

		if($balance_quoted==$balance){
			new MsgConfirm("Balance check passed.");
			return true;
		}
		new Warning("Balance does not match! Quoted: ".ServiceFinancial::centsToEuro($balance_quoted));
		return false;
	}

	private function updateSimilarTransaction(TransaktionBank $new_transaktion, $existing_transaction_id)
	{
		$db = DatabaseHandler::getDefaultDb();
		$db->updateByArray(
			TransaktionBank::table_name,
			array(
				TransaktionBank::FIELD_verwendungszweck => $new_transaktion->getVerwendungszweck(),
				TransaktionBank::FIELD_hash => $new_transaktion->getHash(),
			),
			array(
				new WhereEquals(Model::FIELD_id, $existing_transaction_id),
			)
		);
	}

	public function getCss()
	{
		return array(ModuleGiro::getCss(),
			new Stylesheet(
				ConfigServer::$HTTP_MODULE_ROOT[ModuleGiro::$module_id] . '/v2/css/'.ConfigProject::$SKIN_ID.'/import.css',
				"",
				ConfigServer::$HTTP_MODULE_ROOT[ModuleGiro::$module_id] . '/v2/css/'.ConfigProject::$SKIN_ID.'/mobile_import.css'
			),
		);
	}

	/**
	 * @return string[]|null
	 */
	public function getJsUrls()
	{
		return array(
			ConfigServer::$HTTP_MODULE_ROOT[ModuleGiro::$module_id] . '/v2/giro.js',
		);
	}

}