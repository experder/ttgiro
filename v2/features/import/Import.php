<?php

namespace ttgiro\v2\features\import;

use tt\features\database\v1\DatabaseHandler;
use tt\features\database\v1\Model;
use tt\features\database\v1\WhereEquals;
use tt\features\htmlpage\components\Form;
use tt\features\htmlpage\components\FormInputCheckbox;
use tt\features\htmlpage\components\FormInputDropdown;
use tt\features\htmlpage\components\FormInputText;
use tt\features\htmlpage\components\FormInputTextarea;
use tt\features\htmlpage\components\FormSubset;
use tt\features\htmlpage\components\HtmlComponent;
use tt\features\htmlpage\components\HtmlDiv;
use tt\features\i18n\Trans;
use tt\features\messages\v2\Message;
use tt\features\messages\v2\MsgWarning;
use tt\services\ServiceEnv;
use tt\services\ServiceFinancial;
use tt\services\ServiceStrings;
use tt\services\UNI;
use tt\services\UnicodeIcons;
use ttgiro\v2\GiroConfig;
use ttgiro\v2\model\KontoBuchung;
use ttgiro\v2\model\TransaktionBank;
use ttgiro\v2\model\TransaktionBuchung;

abstract class Import
{

	const GUINAME_FORMAT = "Format";

	/**
	 * @var string $file
	 */
	protected $file = null;
	protected $konto_id = null;
	/**
	 * @var TransaktionBank[] $transactions
	 */
	protected $transactions = null;
	protected $latest_balance = null;

	public function fromImportPost()
	{
		$file = ServiceEnv::valueFromPost(ViewImport::POSTVAL_PREVIEW_FILE);
		$this->file = ServiceStrings::ttTemplates($file);

		$this->konto_id = ServiceEnv::valueFromPost(ViewImport::POSTVAL_KONTO);
	}

	abstract function setLatestTransactions();

	public function getPreviewHtml(){
		$db = DatabaseHandler::getDefaultDb();

		$new_transactions = array();
		$existing_transactions_counter = 0;

		$skip = GiroConfig::$CFG_IMPORT_SKIP;
		foreach ($this->transactions as $transaction) {
			if ($skip-- > 0) continue;
			$hash = $transaction->calcHash();
			$existing_transaction = $db->generalQuery(
				array(Model::FIELD_id),
				TransaktionBank::table_name, array(
				new WhereEquals(TransaktionBank::FIELD_hash, $hash),
				new WhereEquals(TransaktionBank::FIELD_konto, $this->konto_id),
			));
			if ($existing_transaction) {
				$existing_transactions_counter++;
			} else {
				$new_transactions[] = $transaction;
			}
		}

		$form = $this->newTransactionsForm($new_transactions, $this->konto_id);

		if (!$new_transactions) {
			if (ServiceEnv::valueFromPost(ViewImport::POSTVAL_PREVIEW_DELETE)) {
				$form->setSubmitText("Delete file");
			} else {
				$form = false;
			}
		}

		return Trans::late("Existing transactions") . ": $existing_transactions_counter"
			. ($form ? $form->toHtml() : "");
	}

	public function newTransactionsFormInitial($deletefile=true)
	{
		$form = new Form(ViewImport::CMD_EXECUTE);
		$form->addClass("giro_import_preview_form");
		$form->addHiddenField(ViewImport::POSTVAL_FORMAT,
			ServiceEnv::valueFromPost(ViewImport::POSTVAL_FORMAT));
		$form->addHiddenField(ViewImport::POSTVAL_KONTO,
			ServiceEnv::valueFromPost(ViewImport::POSTVAL_KONTO));
		if($this->latest_balance!==null){
			$form->addHiddenField(ViewImport::POSTVAL_EXECUTE_LAST_BALANCE,$this->latest_balance);
		}

		if($deletefile){
			$deleteFlag = ServiceEnv::valueFromPost(ViewImport::POSTVAL_PREVIEW_DELETE);
			if ($deleteFlag) {
				$form->addHiddenField(ViewImport::POSTVAL_EXECUTE_DELETEFILE,
					ServiceStrings::ttTemplates(ServiceEnv::valueFromPost(ViewImport::POSTVAL_PREVIEW_FILE))
				);
			}
		}

		return $form;
	}

	public static function newTransactionsFormFieldset($id,
													   $konto_id=false,
													   TransaktionBank $transaction=null,
													   $onkeyup=""
	){
		$readonly = $transaction!==null;
		if($transaction===null){
			$transaction=new TransaktionBank();
			$transaction->setDatum(date("Y-m-d"));
		}
		$options = TransaktionBuchung::getAlleBuchungskonten(true);
		$subset = new FormSubset();
		$subset->addKeyVal("dataid", $id);

		$subset->add($datum = new FormInputText(
			ViewImport::POSTVAL_EXECUTE_DATUM . $id,
			$transaction->getGuiNameOfColumn(TransaktionBank::FIELD_datum),
			$transaction->getDatumReadable()));
		if($readonly)$datum->setReadonly();

		$subset->add($betrag = new FormInputText(
			ViewImport::POSTVAL_EXECUTE_BETRAG . $id,
			$transaction->getGuiNameOfColumn(TransaktionBank::FIELD_betrag),
			ServiceFinancial::centsToEuro($transaction->getBetrag())));
		if($readonly)$betrag->setReadonly();
		if ($transaction->getBetrag() != 0) $betrag->addClass("euroamount");
		if ($transaction->getBetrag() < 0) $betrag->addClass("negative");

		$subset->add($text = new FormInputTextarea(
			ViewImport::POSTVAL_EXECUTE_VERWENDUNGSZWECK . $id,
			$transaction->getGuiNameOfColumn(TransaktionBank::FIELD_verwendungszweck),
			$transaction->getVerwendungszweck()
		));
		if($readonly)$text->setReadonly();
		if($onkeyup)$text->addKeyVal(HtmlComponent::KEY_ONKEYUP, $onkeyup);

		if($konto_id){
			self::subsetAddSimilarTransactions($transaction, $subset, $konto_id, $id);
		}

		$subset->add(new FormInputDropdown(ViewImport::POSTVAL_EXECUTE_KATEGORIE . $id, $options,
			Trans::late(KontoBuchung::gui_name)));

		return $subset;
	}

	/**
	 * @param TransaktionBank[] $transactions
	 * @return Form
	 */
	private function newTransactionsForm(array $transactions, $konto_id)
	{
		$form = $this->newTransactionsFormInitial();

		$counter = 1;
		foreach ($transactions as $transaction) {
			$id = $counter++;
			$form->add(self::newTransactionsFormFieldset($id, $konto_id, $transaction));
		}

		return $form;
	}

	private static function subsetAddSimilarTransactions(TransaktionBank $transaction, FormSubset $subset,
												  $konto_id, $form_counter)
	{
		$db = DatabaseHandler::getDefaultDb();
		$result = $db->generalQuery(array(
			Model::FIELD_id,
			TransaktionBank::FIELD_verwendungszweck,
		), TransaktionBank::table_name, array(
			new WhereEquals(TransaktionBank::FIELD_datum, $transaction->getDatum()),
			new WhereEquals(TransaktionBank::FIELD_betrag, $transaction->getBetrag()),
			new WhereEquals(TransaktionBank::FIELD_konto, $konto_id),
		));
		if (!$result) return;

		$html = array();

		foreach ($result as $row) {
			$id = $row[Model::FIELD_id];
			$checkbox = new FormInputCheckbox(ViewImport::POSTVAL_EXECUTE_OVERRIDE . $form_counter, false);
			$checkbox
				->addKeyVal("value", $id)
				->addKeyVal(HtmlComponent::KEY_TITLE, 'Override')
				->addClass('override_similar');
			$warntext = "Similar transaction found"
				. " (#$id)!"
				. "\n" . $row[TransaktionBank::FIELD_verwendungszweck];
			$warn = Message::messageHtml(MsgWarning::CSS_TYPE, UNI::asEmoji(UnicodeIcons::Warning_Sign),
				"<pre class='msgOnlyPre'>".htmlentities($warntext)."</pre>");

			$html[] = $checkbox->toHtml() . $warn;
		}

		$subset->add($div = new HtmlDiv(implode("\n", $html)));
		$div->addClass("similar_transaction");
	}

	/**
	 * @return string
	 */
	abstract function getImportHandlerId();

	/**
	 * @return string
	 */
	abstract function getImportHandlerDesc();

	public function transactionPostprocess(TransaktionBank $transaktion)
	{
	}

}