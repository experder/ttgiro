<?php

namespace ttgiro\v2\features\import;

use tt\features\debug\errorhandler\v1\Error;
use tt\services\ServiceFiles;
use tt\services\ServiceFinancial;
use ttgiro\v2\model\TransaktionBank;

class Import_VW extends Import
{

	function setLatestTransactions()
	{
		$content = ServiceFiles::get_contents($this->file);
		$content_array = explode("\r\n", $content);

		//Plausi: Kontostand (Zeile 4):
		if(!preg_match("/^Saldo \\(EUR\\);(.*?);;;;;;;;;;;;;\$/", $content_array[3], $matches)){
			new Error("VW format has changed or file is corrupt. Kontostand.");
		}
		$this->latest_balance = ServiceFinancial::euroToCents($matches[1]);

		//Plausi: Zeile [offset] enthält Spaltenbeschriftungen:
		$offset = 7;
		if(count($content_array)<$offset)new Error("Format has changed or file is corrupt. Lines.");
		if ($content_array[$offset - 1] !== 'Nr.;Buchungsdatum;Umsatzart;Umsatzinformation;UCI;Mandat ID'
			. ';Abweichender Debitor;Abweichender Kreditor;Referenznummer;Wertstellung;Soll (EUR);Haben (EUR)') {
			new Error("Format has changed or file is corrupt. Header.");
		}

		$transaction_array = array();
		for ($i = $offset; $i < count($content_array); $i++) {
			$row = $content_array[$i];

			//Plausi: CSV-Format "";... mit 12 Spalten:
			if (!preg_match("/^"
				. "(?<nr>\\d+?);"
				. "\"(?<Buchungsdatum>.*?)\";"
				. "\"(?<Umsatzart>.*?)\";"
				. "\"(?<Umsatzinformation>.*?)\";"
				. "\"(?<UCI>.*?)\";"
				. "\"(?<mandat>.*?)\";"
				. "\"(?<abweichenderDebitor>.*?)\";"
				. "\"(?<abweichenderKreditor>.*?)\";"
				. "\"(?<Referenznummer>.*?)\";"
				. "\"(?<Wertstellung>.*?)\";"
				. "\"(?<soll>.*?)\";"
				. "\"(?<haben>.*?)\""
				. "\$/", $row, $matches)) new Error("Format has changed or file is corrupt. Line " . ($i + 1));

			//Plausi: Betrag: -1000,00
			$betrag = $matches['haben'] ?: ('-' . $matches['soll']);
			if (!preg_match("/^-?\\d+,\\d{2}\$/", $betrag)) {
				new Error("Invalid amount: $betrag");
			}
			$betrag = str_replace(",", ".", $betrag) * 100;

			//Plausi: Datum: 13.01.2023
			$datum = $matches['Buchungsdatum'];
			if (!preg_match("/^\\d{2}\\.\\d{2}\\.\\d{4}\$/", $datum)) new Error("Invalid date '$datum'!");
			$datum = date("Y-m-d", strtotime($datum));

			$text = $matches['Umsatzart']
				. "\n" . $matches['Umsatzinformation'];

			$transaction = TransaktionBank::fromImport($datum, $betrag, $text);

			$transaction_array[] = $transaction;
		}

		$this->transactions=$transaction_array;
	}

	/**
	 * @return string
	 */
	function getImportHandlerId()
	{
		return "vwfs";
	}

	/**
	 * @return string
	 */
	function getImportHandlerDesc()
	{
		return "VW";
	}

}