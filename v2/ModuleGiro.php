<?php

namespace ttgiro\v2;

use tt\features\autoloader\simple1\RootNamespace;
use tt\features\config\v1\ConfigDatabase;
use tt\features\config\v1\ConfigProject;
use tt\features\config\v1\ConfigServer;
use tt\features\css\Stylesheet;
use tt\features\database\v1\Model;
use tt\features\frontcontroller\bycode\Route;
use tt\features\i18n\Trans;
use tt\features\modulehandler\v1\RegistrationHandler;
use tt\services\polyfill\Php7;
use ttgiro\v2\features\import\ViewImport;
use ttgiro\v2\model\schema\AbschluesseBank2;
use ttgiro\v2\model\schema\AbschluesseBuchung2;
use ttgiro\v2\model\schema\Bilanzraum2;
use ttgiro\v2\model\schema\DkbHashLog2;
use ttgiro\v2\model\schema\KontoBank2;
use ttgiro\v2\model\schema\KontoBuchung2;
use ttgiro\v2\model\schema\TransaktionBank2;
use ttgiro\v2\model\schema\TransaktionBuchung2;
use ttgiro\v2\views\ViewBankCheck;
use ttgiro\v2\views\ViewBankOverview;
use ttgiro\v2\views\ViewBankAccount;
use ttgiro\v2\views\ViewBookingOverview;

class ModuleGiro extends RegistrationHandler
{

	public static $module_id = "ttgiro";
	const GUI_NAME = "Household budget";
	private static $namespace_root = 'ttgiro\\v2';

	/**
	 * @return Route[]
	 */
	function getModuleRoutes() {
		return array(
			new Route("/giro/import", ViewImport::getClass(), ViewImport::title()),
			new Route("/giro/bank/overview", ViewBankOverview::getClass(), ViewBankOverview::title()),
			new Route("/giro/bank/account", ViewBankAccount::getClass(), false),
			new Route("/giro/bank/check", ViewBankCheck::getClass(), ViewBankCheck::title()),
			new Route("/giro/booking/overview", ViewBookingOverview::getClass(), ViewBookingOverview::title()),

			new Route("/giro/api", ApiEndpoint::getClass(), false),
		);
	}

	/**
	 * @return RootNamespace[]|null
	 */
	function getNamespaceRoots() {
		return array(
			new RootNamespace(self::$namespace_root, __DIR__),
		);
	}

	/**
	 * @return Model[]|string[]
	 */
	function createModelSchema() {
		return array(
//			new Bilanzraum(),
//			new KontoBank(),
//			new KontoBuchung(),
//			new TransaktionBank(),
//			new TransaktionBuchung(),
//			new DkbHashLog(),
//			new AbschluesseBank(),
//			new AbschluesseBuchung(),

			Bilanzraum2::getClass(),
			KontoBank2::getClass(),
			KontoBuchung2::getClass(),
			TransaktionBank2::getClass(),
			TransaktionBuchung2::getClass(),
			DkbHashLog2::getClass(),
			AbschluesseBank2::getClass(),
			AbschluesseBuchung2::getClass(),
		);
	}

	/**
	 * @return string
	 */
	function getModuleId() {
		return self::$module_id;
	}

	/**
	 * @param string $idString ModuleGiro::DBCFG_
	 * @return string|false
	 * @noinspection PhpUnused
	 */
	public static function getDbCfg($idString) {
		return ConfigDatabase::getValue(ModuleGiro::$module_id, $idString);
	}

	public function getI18namespace() {
		return self::$namespace_root . '\i18n';
	}

	public function getGuiName() {
		if (ConfigProject::$SKIN_BILDER_SAGEN_MEHR_ALS_WORTE) {
			return Php7::mb_chr(UnicodeIcons::money_with_wings);
		}
		return Trans::late(self::GUI_NAME);
	}

	public static function getCss() {
		return new Stylesheet(
			ConfigServer::$HTTP_MODULE_ROOT[ModuleGiro::$module_id] . '/v2/css/' . ConfigProject::$SKIN_ID . '/giro.css',
			ConfigServer::$HTTP_MODULE_ROOT[ModuleGiro::$module_id] . '/v2/css/' . ConfigProject::$SKIN_ID . '/nightmode_giro.css',
			ConfigServer::$HTTP_MODULE_ROOT[ModuleGiro::$module_id] . '/v2/css/' . ConfigProject::$SKIN_ID . '/mobile_giro.css'
		);
	}

}