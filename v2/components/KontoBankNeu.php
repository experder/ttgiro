<?php

namespace ttgiro\v2\components;

use tt\features\database\v2\Model;
use ttgiro\v2\model\schema\KontoBank2;

class KontoBankNeu extends Model
{

	/**
	 * @var string $name
	 */
	private $name;

	public static function fromId($id){
		$entity = new KontoBankNeu();
		return $entity->fromDbById($id);
	}

	/**
	 * @return string
	 */
	function getSchemaClass() {
		return KontoBank2::getClass();
	}

	/**
	 * @param string[] $row
	 * @return $this
	 */
	public function fromDbRow(array $row) {
		$this->name = $row[KontoBank2::COL_name];
		return $this;
	}

	/**
	 * @param string $name
	 * @return KontoBankNeu
	 */
	public function setName($name) {
		$this->name = $name;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getName() {
		return $this->name;
	}

}