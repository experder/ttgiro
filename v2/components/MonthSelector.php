<?php

namespace ttgiro\v2\components;

use DateInterval;
use DateTime;
use Exception;
use tt\features\debug\errorhandler\v1\Error;
use tt\features\htmlpage\components\Html_A;
use tt\services\polyfill\Php7;
use tt\services\ServiceDateTime;
use tt\services\ServiceEnv;
use tt\services\UnicodeIcons;

class MonthSelector
{

	const GETVAL_month='month';

	/**
	 * @var string $month YYYY-MM
	 */
	protected $month;
	protected $vormonat;
	protected $folgemonat;

	public function __construct($month=true)
	{
		if($month===true)$month=ServiceEnv::valueFromGet(MonthSelector::GETVAL_month)?:date("Y-m");
		$this->init($month);
	}

	private function init($month){
		if (!$month) new Error("No month provided!");
		if (!preg_match("/^20\\d{2}-\\d{2}\$/", $month)) new Error("Not a valid month: '$month'!");
		$this->month = $month;
		$this->calcVormonatAndFolgemonat();
	}

	private function calcVormonatAndFolgemonat()
	{
		$this->vormonat = ServiceDateTime::vormonat($this->month);
		$this->folgemonat = ServiceDateTime::folgemonat($this->month);
	}

	/**
	 * @return string YYYY-MM
	 */
	public function getVormonat()
	{
		return $this->vormonat;
	}

	/**
	 * @return string YYYY-MM
	 */
	public function getMonth()
	{
		return $this->month;
	}

	public function getMonthYMD()
	{
		return $this->month."-01";
	}
	public function getFolgemonatYMD()
	{
		return $this->folgemonat."-01";
	}

	protected function checkVormonat(){
		return true;
	}
	protected function checkFolgemonat(){
		return true;
	}

	public function viaGetval()
	{
		$prev = $this->checkVormonat()?ServiceEnv::updateUrlParam(self::GETVAL_month, $this->vormonat):false;
		$next = $this->checkFolgemonat()?ServiceEnv::updateUrlParam(self::GETVAL_month, $this->folgemonat):false;
		$html = $this->dateHeaderWithNavigation($this->month, $prev, $next);
		return "<h2>$html</h2>";
	}

	private function dateHeaderWithNavigation($month, $prev, $next)
	{
		$header = ServiceDateTime::dateAusgeschrieben($month);
		$left = $prev ?
			"<a href='" . $prev . "'"
			." class='" . Html_A::CLASS_BUTTON . "'>"
			. Php7::mb_chr(UnicodeIcons::White_Left_Pointing_Backhand_Index)
			. "</a>" : "";
		$right = $next ?
			"<a href='" . $next . "'"
			." class='" . Html_A::CLASS_BUTTON . "'>"
			. Php7::mb_chr(UnicodeIcons::White_Right_Pointing_Backhand_Index)
			. "</a>" : "";
		return $left . ' ' . $header . " " . $right;
	}

}