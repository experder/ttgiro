<?php

namespace ttgiro\v2\components;

use tt\features\config\v1\CFG_S;
use tt\features\database\v1\DatabaseHandler;
use tt\features\database\v1\views\EditModel;
use tt\features\database\v1\WhereEquals;
use tt\features\database\v2\Model;
use tt\features\database\v2\Schema;
use tt\features\debug\errorhandler\v1\Error;
use tt\features\frontcontroller\bycode\ServiceRoutes;
use tt\features\i18n\Trans;
use tt\services\DEBUG;
use tt\services\ServiceEnv;
use tt\services\ServiceFinancial;
use tt\services\UNI;
use tt\services\UnicodeIcons;
use ttgiro\v2\model\KontoBank;
use ttgiro\v2\model\schema\TransaktionBank2;
use ttgiro\v2\model\TransaktionBank;
use ttgiro\v2\model\TransaktionBuchung;

class TransactionBank extends Model
{

	/**
	 * @var int $id
	 */
	private $id;
//	/**
//	 * @var int $konto_id
//	 */
//	private $konto_id;
//	/**
//	 * @var KontoBankNeu $konto
//	 */
//	private $konto;
	/**
	 * @var int $betrag
	 */
	private $betrag;
	/**
	 * @var string $datum
	 */
	private $datum;
	/**
	 * @var string $verwendungszweck
	 */
	private $verwendungszweck;

	/**
	 * @param string[] $row
	 * @return TransactionBank
	 */
	public static function fromRow(array $row){
		$entity = new TransactionBank();
		return $entity->fromDbRow($row);
	}

	public static function fromId($id){
		$entity = new TransactionBank();
		return $entity->fromDbById($id);
	}

//	/**
//	 * @param KontoBankNeu $konto
//	 * @return TransactionBank
//	 */
//	public function setKonto($konto) {
//		$this->konto = $konto;
//		return $this;
//	}

	/**
	 * @return string
	 */
	function getSchemaClass() {
		return TransaktionBank2::getClass();
	}

	/**
	 * @param string[] $row
	 * @return $this
	 */
	public function fromDbRow(array $row) {
		$this->id = $row[Schema::COL_id];
		$this->betrag = $row[TransaktionBank2::COL_betrag];
		$this->datum = $row[TransaktionBank2::COL_datum];
		$this->verwendungszweck = $row[TransaktionBank2::COL_verwendungszweck];
		#if(isset($row[TransaktionBank2::COL_konto]))$this->konto_id = $row[TransaktionBank2::COL_konto];
		return $this;
	}

	/**
	 * @return string
	 */
	public function transactionHtml()
	{
		$betrag_cent = $this->betrag;
		$betrag_ger = ServiceFinancial::centsToEuro($betrag_cent);
		$betrag_class = "euroamount" . ($betrag_cent < 0 ? " negative" : "");

		$datum_ger = date("d.m.Y", strtotime($this->datum));

		$text = $this->verwendungszweck;
		$text = trim($text);
		$text = ServiceEnv::normalizeLinebreaks($text);
		$text = htmlentities($text);
		$text = preg_replace("/\\n\\W*\\n/", "\n<div class='trimmed_linebreak'></div>", $text);

		$buchungskonto_transaktionen = $this->transactionBuchungskontoHtml();

		return "<div class='transaction'>"
			. "<pre class='datum'>" . $datum_ger . "</pre>"
			. "<pre class='$betrag_class'>" . $betrag_ger . "</pre>"
			. "<div class='textbody'>"
			. "<pre class='verwendungszweck'>" . $text . "</pre>"
			. $buchungskonto_transaktionen
			. "</div>"
			. "</div>";
	}

	private function transactionBuchungskontoHtml()
	{
		$transaction_bankkonto_id = $this->id;
		$betrag_banktransaktion = $this->betrag;
		$datum_banktransaktion = $this->datum;

		$db = DatabaseHandler::getDefaultDb();
		$query_buchungen = $db->generalQuery(
			array(
				\tt\features\database\v1\Model::FIELD_id,
				TransaktionBuchung::FIELD_datum,
				TransaktionBuchung::FIELD_betrag,
				TransaktionBuchung::FIELD_verwendungszweck,
				TransaktionBuchung::FIELD_buchungskonto,
				TransaktionBuchung::FIELD_gegenbuchung,
				TransaktionBuchung::FIELD_await,
			),
			TransaktionBuchung::table_name,
			array(
				new WhereEquals(TransaktionBuchung::FIELD_transaktion_bank, $transaction_bankkonto_id),
			)
		);

		if (!$query_buchungen) {
			new Error("Keine Buchung gefunden für Bankkonto-Transaktion #$transaction_bankkonto_id");
		}
		$html = array();
		$summe_betraege = 0;
		foreach ($query_buchungen as $buchung) {
			$buchung_id = $buchung[\tt\features\database\v1\Model::FIELD_id];
			$datum_buchung = $buchung[TransaktionBuchung::FIELD_datum];
			$datum = $datum_buchung === $datum_banktransaktion ? "" : $datum_buchung;

			$betrag_buchung = $buchung[TransaktionBuchung::FIELD_betrag];
			$betrag = ($betrag_buchung === $betrag_banktransaktion && count($query_buchungen) === 1)
				? false
				: $betrag_buchung;
			$summe_betraege += $betrag_buchung;

			$verwendungszweck = $buchung[TransaktionBuchung::FIELD_verwendungszweck];
			if (DEBUG::verbose()) $verwendungszweck = "[" . $buchung[\tt\features\database\v1\Model::FIELD_id] . "] " . $verwendungszweck;

			$buchungskonto_id = $buchung[TransaktionBuchung::FIELD_buchungskonto];
			$buchungskonto_name = $buchungskonto_id
				? TransaktionBuchung::getBuchungskontoName($buchungskonto_id)
				: false;

			$gegenbuchungen_html = $this->gegenbuchungenHtml(
				$buchung[TransaktionBuchung::FIELD_gegenbuchung],
				$buchung[TransaktionBuchung::FIELD_await],
				$betrag_buchung,
				$buchung_id,
				$buchungskonto_id,
				$gegenbuchung_inkonsistent
			);
			if ($gegenbuchungen_html !== false) {
				$gegenbuchungen_html = "<div class='buchungsdatum'></div><div class='buchungsbetrag'></div>"
					. "<div class='gegenbuchungen'>" . $gegenbuchungen_html . "</div>";
			}

			if (!$buchungskonto_name) {
				if ($gegenbuchungen_html === false && $betrag_buchung) {
					$buchungskonto_name = "<span class='inconsistency'>".Trans::late("No booking account")."!</span>";
					$buchungskonto_name .= TransaktionBuchung::htmlSelector($buchung_id);
				} else {
					$buchungskonto_name = "(".Trans::late("no booking account").")";
				}
			}

			$html[] = $this->buchungHtml(
				$gegenbuchungen_html,
				$datum,
				$betrag,
				$buchungskonto_name,
				$verwendungszweck,
				$buchung_id
			);
		}
		if ($summe_betraege != $betrag_banktransaktion) {
			new Error("Summe der Teilbeträge ist "
				. ($summe_betraege < $betrag_banktransaktion ? "zu niedrig" : "zu hoch")
				. "! #$transaction_bankkonto_id");
		}
		return implode("\n", $html);
	}

	private function buchungHtml($gegenbuchungen_html, $datum_sql, $betrag_cent, $buchungskonto_name,
								 $verwendungszweck, $buchung_id)
	{
		$datum = $datum_sql ? date("d.m.", strtotime($datum_sql)) : "";
		$betrag = $betrag_cent === false ? "" : ServiceFinancial::centsToEuro($betrag_cent);
		$verwendungszweck = $verwendungszweck ? htmlentities($verwendungszweck) : "";
		return "<div class='buchungen'>"
			. "<div class='buchungsdatum'>$datum</div>"
			. "<div class='buchungsbetrag'>$betrag</div>"
			. "<div class='buchungskonto'>$buchungskonto_name</div>"
			. "<div class='buchung_text'>$verwendungszweck " . TransaktionBuchung::htmlEditOption($buchung_id) . "</div>"
			. $gegenbuchungen_html
			. "</div>";
	}

	private function gegenbuchungenHtml($gegenbuchung_id, $await, $betrag, $id, $buchungskonto, &$inkonsistenz = false)
	{
		if (!$gegenbuchung_id && !$await) return false;

		if ($await) {
			if ($gegenbuchung_id) {
				new Error("Inconsistent database!299"
					. (CFG_S::$DEVMODE
						? "\nSELECT * FROM giro_transaktion_buchung where await and gegenbuchung is not null"
						: "")
				);
			}
			if ($betrag === 0) return "(".Trans::late("pending offsetting entry").")";
			$inkonsistenz = true;
			return "<span class='inconsistency'>".Trans::late("Pending offsetting entry")."!</span>";
		}

		if ($gegenbuchung_id == $id) {
			new Error("Inconsistent database!326"
				. (CFG_S::$DEVMODE ? "\nSELECT * FROM giro_transaktion_buchung where gegenbuchung=id" : "")
			);
		}

		$gegenbuchung = new TransaktionBuchung();
		$gegenbuchung->fromDbWhereEqualsId($gegenbuchung_id);

		if ($gegenbuchung->getGegenbuchung() !== $id) {
			new Error("Inconsistent database!311"
				. (CFG_S::$DEVMODE
					? "\nSELECT b.id,b.gegenbuchung,g.gegenbuchung gegengegen FROM giro_transaktion_buchung b
left join giro_transaktion_buchung g on b.gegenbuchung = g.id
where b.gegenbuchung is not null
and (g.gegenbuchung is null or g.gegenbuchung!=b.id)"
					: "")
			);
		}

		if ($gegenbuchung->getBetrag() * -1 != $betrag) {
			new Error("Inconsistent database!329"
				. (CFG_S::$DEVMODE ? "\nSELECT b.*,g.betrag gegenbetrag FROM giro_transaktion_buchung b
left join giro_transaktion_buchung g on b.gegenbuchung = g.id
where g.betrag is not null
and b.betrag*-1!=g.betrag " : "")
			);
		}

		if ($gegenbuchung->getBuchungskonto() !== $buchungskonto) {
			new Error("Different booking accounts (Buchungskonten)!");
		}

		$tranaktionKonto = new TransaktionBank();
		$tranaktionKonto->fromDbWhereEqualsId($gegenbuchung->getTransaktionBank());
		$konto = new KontoBank();
		$konto->fromDbWhereEqualsId($tranaktionKonto->getKonto());

		$gegenbuchung_html = TransaktionBuchung::gegenbuchungEntry(
			$gegenbuchung->getId(),
			$konto->getName(),
			$gegenbuchung->getDatum(),
			$gegenbuchung->getVerwendungszweck() ?: $tranaktionKonto->getVerwendungszweck()
		);

		$a = ServiceRoutes::getLinkComponent(EditModel::getClass(), $gegenbuchung_html, array(
			EditModel::PARAM_classname => TransaktionBuchung::getClass(),
			EditModel::PARAM_id => $gegenbuchung_id,
		));

		return UNI::asEmoji(UnicodeIcons::Leftwards_Arrow_with_Hook)." ".Trans::late("Offsetting entry").": " . $a;

	}
}